//==================================================
// obstacle.h
// Author: Buriya Kota
//==================================================
#ifndef _OBSTACLE_H_
#define _OBSTACLE_H_

//**************************************************
// インクルード
//**************************************************
#include "gimmick.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CObstacle : public CGimmick
{
public:
	// 画面(モード)の種類
	enum OBSTACLE_MODEL_TYPE
	{
		OBSTACLE_MODEL_TYPE_TREE = 0,				// 木
		OBSTACLE_MODEL_TYPE_CLEENNESS_OBJECT,		// 透明オブジェクト
		OBSTACLE_MODEL_TYPE_CLEENNESS_OBJECT_VERTICAL,	// 透明オブジェクト縦
		OBSTACLE_MODEL_TYPE_CLEENNESS_OBJECT_SHORT,				// 透明オブジェクト(短い)
		OBSTACLE_MODEL_TYPE_CLEENNESS_OBJECT_VERTICAL_SHORT,	// 透明オブジェクト縦(短い)
		OBSTACLE_MODEL_TYPE_MAX,					// 最大数
		OBSTACLE_MODEL_TYPE_NONE,					// 使用しない
	};

	explicit CObstacle(int nPriority = PRIORITY_OBJECT);
	~CObstacle();

	HRESULT Init() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;

	static CObstacle *Create(OBSTACLE_MODEL_TYPE type);

	OBSTACLE_MODEL_TYPE GetObstacleModelType() { return m_ObstacleModelType; }
	void SetObstacleModelType(OBSTACLE_MODEL_TYPE type) { m_ObstacleModelType = type; }

private:
	static OBSTACLE_MODEL_TYPE m_ObstacleModelType;
};

#endif	// _OBSTACLE_H_