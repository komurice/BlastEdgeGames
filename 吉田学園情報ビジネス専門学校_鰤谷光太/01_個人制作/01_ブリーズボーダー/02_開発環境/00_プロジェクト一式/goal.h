//==================================================
// goal.h
// Author: Buriya Kota
//==================================================
#ifndef _GOAL_H_
#define _GOAL_H_

//**************************************************
// インクルード
//**************************************************
#include "gimmick.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CGoal : public CGimmick
{
public:
	explicit CGoal(int nPriority = PRIORITY_OBJECT);
	~CGoal();

	HRESULT Init() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode);

	static CGoal *Create();

	bool CollisionGoal(D3DXVECTOR3* pPos, D3DXVECTOR3* pPosOld, D3DXVECTOR3* pSize);

	bool IsGoal() { return m_bIsLanding; }
private:
	bool m_bIsLanding;
	int m_nTime;
};

#endif	// _GIMMICK_H_