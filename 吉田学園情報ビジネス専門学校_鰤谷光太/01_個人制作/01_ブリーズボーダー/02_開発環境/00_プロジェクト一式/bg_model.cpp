//==================================================
// bg_model.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "bg_model.h"

#include "model_data.h"

//**************************************************
// 静的メンバ変数
//**************************************************
CBgModel::BG_MODEL_TYPE CBgModel::m_bgModelType;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CBgModel::CBgModel(int nPriority) : CGimmick(nPriority)
{
	SetStencil(true);
}

//--------------------------------------------------
//	デストラクタ
//--------------------------------------------------
CBgModel::~CBgModel()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CBgModel::Init()
{
	CObjectX::Init();

	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CBgModel::Update()
{
	CObjectX::Update();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CBgModel *CBgModel::Create(BG_MODEL_TYPE type)
{
	CBgModel *pBgModel = nullptr;
	pBgModel = new CBgModel;
	m_bgModelType = type;

	pBgModel->Init();

	switch (type)
	{
	case BG_MODEL_TYPE_TREE:
		pBgModel->SetModelData(CModelData::MODEL_TREE);

		break;

	default:
		assert(false);
		break;
	}

	return pBgModel;
}
