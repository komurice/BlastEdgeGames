//==================================================
// goal.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "manager.h"
#include "game.h"

#include "model_data.h"
#include "fade.h"
#include "player3D.h"

#include "goal.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CGoal::CGoal(int nPriority) : CGimmick(nPriority)
{
	SetModelData(CModelData::MODEL_GOAL);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CGoal::~CGoal()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CGoal::Init()
{
	CGimmick::Init();
	m_bIsLanding = false;
	m_nTime = 0;
	SetStencil(false);

	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CGoal::Update()
{
	CGimmick::Update();
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CGoal::Draw(DRAW_MODE drawMode)
{
	if (drawMode == DRAW_MODE_SHADOW)
	{
		if (CGame::GetGame()->GetPlayer3D() != nullptr)
		{
			D3DXVECTOR3 diff = GetPos() - CGame::GetGame()->GetPlayer3D()->GetPos();

			if (D3DXVec3LengthSq(&diff) < 5000.0f * 5000.0f)
			{
				CObjectX::Draw(drawMode);
			}
		}
	}
	else
	{
		CObjectX::Draw(drawMode);
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CGoal *CGoal::Create()
{
	CGoal *pGoal;
	pGoal = new CGoal;

	if (pGoal != nullptr)
	{
		pGoal->Init();
	}
	else
	{
		assert(false);
	}

	return pGoal;
}

//--------------------------------------------------
// 当たり判定(矩形)
//--------------------------------------------------
bool CGoal::CollisionGoal(D3DXVECTOR3 * pPos, D3DXVECTOR3 * pPosOld, D3DXVECTOR3 * pSize)
{
	CModelData* modelData = CManager::GetManager()->GetModelData();

	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 min = modelData->GetModel(GetModelData()).vtxMin;
	D3DXVECTOR3 max = modelData->GetModel(GetModelData()).vtxMax;

	// 奥　手前
	if (pPos->x  < pos.x + max.x + pSize->x / 2.0f
		&& pPos->x > pos.x + min.x - pSize->x / 2.0f)
	{
		if (pPosOld->z >= pos.z + max.z + pSize->z / 2.0f
			&& pPos->z < pos.z + max.z + pSize->z / 2.0f)
		{// 奥
			m_bIsLanding = true;
		}
		if (pPosOld->z <= pos.z + min.z - pSize->z / 2.0f
			&& pPos->z > pos.z + min.z - pSize->z / 2.0f)
		{// 手前
			m_bIsLanding = true;
		}
	}

	return m_bIsLanding;
}
