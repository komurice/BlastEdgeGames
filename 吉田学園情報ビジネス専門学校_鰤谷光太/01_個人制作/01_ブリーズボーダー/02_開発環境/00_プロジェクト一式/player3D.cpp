//==================================================
// player3D.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "utility.h"

#include "game.h"

#include "manager.h"
#include "camera.h"
#include "sound.h"
#include "input.h"
#include "debug_proc.h"

#include "score.h"
#include "object3D.h"
#include "player3D.h"
#include "shadow.h"
#include "meshfield.h"
#include "goal.h"
#include "particle.h"
#include "item.h"
#include "obstacle.h"

//**************************************************
// マクロ定義
//**************************************************
#define MAX_SPEED			(10.0f)
#define FRICTION			(0.07f)
#define GRAVITY				(0.5f)
#define MAX_ACCEL			(1.0f)
#define MIN_ACCEL			(0.0f)
#define COIN_PARTICLE		(5)
#define NUM_GOAL			(2)
#define KNOCKBACK			(20.0f)

//**************************************************
// 静的メンバ変数
//**************************************************
const float CPlayer3D::PLAYER_SPEED = 2.0f;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CPlayer3D::CPlayer3D(int nPriority) : CObject(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CPlayer3D::~CPlayer3D()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CPlayer3D::Init()
{
	LoadSetFile("data/TEXT/motion.txt");

	m_nCurrentKey = 0;

	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_posOld = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rotDest = D3DXVECTOR3(0.0f, D3DX_PI, 0.0f);
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_size = D3DXVECTOR3(50.0f, 50.0f, 50.0f);
	m_accel = 0.0f;
	m_nGoalTimer = 0;
	D3DXQuaternionIdentity(&m_myQuaternion);
	m_bIsMesh = false;
	m_bIsHitObstacle = false;
	m_bIslandingGoal = false;
	m_nStatusRecoveryTime = 0;
	m_fMeahAngle = 0.0f;
	m_fMeshMaxAngle = 0.0f;

	// モデルの生成
	m_pModel[0] = CModel::Create(CModelData::MODEL_PLAYER_BOARD);
	m_pModel[1] = CModel::Create(CModelData::MODEL_PLAYER_BODY);
	m_pModel[2] = CModel::Create(CModelData::MODEL_PLAYER_HEAD);
	m_pModel[3] = CModel::Create(CModelData::MODEL_PLAYER_RIGHT_ARM);
	m_pModel[4] = CModel::Create(CModelData::MODEL_PLAYER_LEFT_ARM);
	m_pModel[5] = CModel::Create(CModelData::MODEL_PLAYER_LFT_THIGH);
	m_pModel[6] = CModel::Create(CModelData::MODEL_PLAYER_RIGHT_THIGH);
	m_pModel[7] = CModel::Create(CModelData::MODEL_PLAYER_LEFT_LEG);
	m_pModel[8] = CModel::Create(CModelData::MODEL_PLAYER_RIGHT_LEG);

	// 親の設定
	m_pModel[0]->SetParent(nullptr);
	m_pModel[1]->SetParent(m_pModel[0]);
	m_pModel[2]->SetParent(m_pModel[1]);
	m_pModel[3]->SetParent(m_pModel[1]);
	m_pModel[4]->SetParent(m_pModel[1]);
	m_pModel[5]->SetParent(m_pModel[1]);
	m_pModel[6]->SetParent(m_pModel[1]);
	m_pModel[7]->SetParent(m_pModel[5]);
	m_pModel[8]->SetParent(m_pModel[6]);

	m_pModel[0]->SetScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));
	m_pModel[1]->SetScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));
	m_pModel[2]->SetScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));
	m_pModel[3]->SetScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));
	m_pModel[4]->SetScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));
	m_pModel[5]->SetScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));
	m_pModel[6]->SetScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));
	m_pModel[7]->SetScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));
	m_pModel[8]->SetScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));

	m_pModel[0]->SetStencil(true);
	m_pModel[1]->SetStencil(true);
	m_pModel[2]->SetStencil(true);
	m_pModel[3]->SetStencil(true);
	m_pModel[4]->SetStencil(true);
	m_pModel[5]->SetStencil(true);
	m_pModel[6]->SetStencil(true);
	m_pModel[7]->SetStencil(true);
	m_pModel[8]->SetStencil(true);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CPlayer3D::Uninit()
{
	for (int nCnt = 0; nCnt < MAX_PARTS; nCnt++)
	{
		if (m_pModel[nCnt] == nullptr)
		{
			continue;
		}

		m_pModel[nCnt]->Uninit();
		delete m_pModel[nCnt];
		m_pModel[nCnt] = nullptr;
	}

	// deleteフラグを立てる
	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CPlayer3D::Update()
{
	// posOld の更新
	D3DXVECTOR3 posOld = GetPos();
	SetPosOld(posOld);

	// 重力
	m_move.y += -GRAVITY;

	// モーション
	Motion_();
	if (!m_bIslandingGoal)
	{
		// プレイヤーの動き
		Control_();
	}

	// アイテムとの当たり判定
	CollisionItem_();
	// ゴールとの当たり判定
	CollisionGoal_();

	if (!m_bIsHitObstacle)
	{// 障害物との当たり判定
		bool isLanding = CollisionObstacle_();
		if (isLanding)
		{
			D3DXVECTOR3 vecMove;
			D3DXVec3Normalize(&vecMove, &m_move);

			m_move = -vecMove * KNOCKBACK;
			m_bIsHitObstacle = true;
		}
	}

	if (m_bIsHitObstacle)
	{// 当たった状態の時
		m_nStatusRecoveryTime++;
		if (m_nStatusRecoveryTime > 40)
		{// プレイヤーの回復
			m_bIsHitObstacle = false;
			m_nStatusRecoveryTime = 0;
		}
	}

	// 地面（メッシュ）との当たり判定
	CollisionMesh_();

	SetPos(m_pos);
	SetSize(m_size);

#ifdef _DEBUG
	// デバッグ表示
	CDebugProc::Print("プレイヤーの現在の角度 : %f\n", m_rot.y);
	CDebugProc::Print("プレイヤーの現在の位置 : %f,%f,%f\n", m_pos.x, m_pos.y, m_pos.z);
	CDebugProc::Print("プレイヤーの過去の位置 : %f,%f,%f\n", m_posOld.x, m_posOld.y, m_posOld.z);
	CDebugProc::Print("プレイヤーのMOVE : %+f, %+f, %+f\n", m_move.x, m_move.y, m_move.z);
	CDebugProc::Print("プレイヤーの加速度 : %f\n\n", m_accel);
	CDebugProc::Print("メッシュの角度 : %f\n\n", m_fMeahAngle);
	CDebugProc::Print("メッシュの最大角度 : %f\n\n", m_fMeshMaxAngle);
#endif // DEBUG
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CPlayer3D::Draw(DRAW_MODE drawMode)
{
	// 計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans, mtxQuat;
	D3DXQUATERNION quat;
	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&m_mtx);

	// クォータニオンから回転マトリックスを作成
	D3DXMatrixRotationQuaternion(&mtxQuat, &m_myQuaternion);

	// 向きを反映
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);

	// 行列の掛け算
	D3DXMatrixMultiply(&mtxRot, &mtxRot, &mtxQuat);
	D3DXMatrixMultiply(&m_mtx, &m_mtx, &mtxRot);

	// 位置を反映
	D3DXMatrixTranslation(&mtxTrans, m_pos.x, m_pos.y, m_pos.z);
	D3DXMatrixMultiply(&m_mtx, &m_mtx, &mtxTrans);

	for (int nCnt = 0; nCnt < MAX_PARTS; nCnt++)
	{
		if (m_pModel[nCnt] == nullptr)
		{
			continue;
		}

		m_pModel[nCnt]->Draw(&m_mtx, drawMode);
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CPlayer3D* CPlayer3D::Create(D3DXVECTOR3 pos)
{
	CPlayer3D *pPlayer3D = nullptr;
	pPlayer3D = new CPlayer3D;

	if (pPlayer3D != nullptr)
	{
		pPlayer3D->Init();
		pPlayer3D->SetPos(pos);
	}
	else
	{
		assert(false);
	}

	return pPlayer3D;
}

//--------------------------------------------------
// 加速度
//--------------------------------------------------
void CPlayer3D::AddAccel(const float & accel)
{
	m_accel = accel;
}

//--------------------------------------------------
// 現在の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CPlayer3D::RotNormalization(D3DXVECTOR3 rot)
{
	m_rot = rot;

	// 現在の角度の正規化
	if (m_rot.y > D3DX_PI)
	{
		m_rot.y -= D3DX_PI * 2.0f;
	}
	else if (m_rot.y < -D3DX_PI)
	{
		m_rot.y += D3DX_PI * 2.0f;
	}

	return m_rot;
}

//--------------------------------------------------
// 目的の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CPlayer3D::RotDestNormalization(D3DXVECTOR3 rot, D3DXVECTOR3 rotDest)
{
	m_rot = rot;
	m_rotDest = rotDest;

	// 目的の角度の正規化
	if (m_rotDest.y - m_rot.y > D3DX_PI)
	{
		m_rotDest.y -= D3DX_PI * 2.0f;
	}
	else if (m_rotDest.y - m_rot.y < -D3DX_PI)
	{
		m_rotDest.y += D3DX_PI * 2.0f;
	}

	return m_rotDest;
}

//--------------------------------------------------
// 操作
//--------------------------------------------------
void CPlayer3D::Control_()
{
	// インプット
	CInput *pInput = CInput::GetKey();

	D3DXVECTOR3 vec(0.0f, 0.0f, 0.0f);

	D3DXVECTOR3 rotDest = GetRotDest();

	if ((vec.x == 0.0f) && (vec.z == 0.0f))
	{
		if (m_bIsMesh)
		{
			vec.z += 1.0f;
		}

		if (pInput->Press(KEY_LEFT))
		{// 左
			// サウンド
			//CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_CURVE);
			rotDest.y -= (D3DX_PI / 180.0f) * 2.0f;
		}
		if (pInput->Press(KEY_RIGHT))
		{// 右
			// サウンド
			//CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_CURVE);
			rotDest.y += (D3DX_PI / 180.0f) * 2.0f;
		}
		SetRotDest(rotDest);
	}

	D3DXVECTOR3 rot = GetRot();
	// 目的の角度の正規化
	rotDest = RotDestNormalization(rot, rotDest);

	// 目的の角度を現在の角度に近づける
	rot.y += (rotDest.y - rot.y) * 1.0f;

	// 現在の角度の正規化
	rot = RotNormalization(rot);
	SetRot(rot);

	// 速度の算出
	D3DXVec3Normalize(&vec, &vec);
	// 進みたい方向に加算
	m_move += vec * PLAYER_SPEED;

	if (!m_bIsMesh)
	{
		m_accel = 1.0f;
	}

	if (m_bIsMesh)
	{// 摩擦係数を加える
		m_move *= (1.0f - FRICTION);
	}

	// メッシュの角度に合わせて移動方向を変える
	if (m_bIsMesh)
	{// プレイヤーの向いている方向に移動方向を合わせる
		D3DXMATRIX mtxRot;
		D3DXMatrixRotationYawPitchRoll(&mtxRot, rot.y, rot.x, rot.z);

		D3DXVECTOR3 groundDir = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
		D3DXVec3TransformCoord(&groundDir, &groundDir, &mtxRot);
		D3DXVec3Normalize(&groundDir, &groundDir);

		m_move.x += -groundDir.x * 3.0f;
	}

	D3DXVECTOR3 pos = GetPos();
	// 移動量を代入
	pos += m_move;
	SetPos(pos);

	if (m_bIsMesh)
	{// 雪のパーティクル
		CParticle* particle = CParticle::Create(D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(10.0f, 10.0f, 0.0f), 50);
		particle->SetPos(pos);
		particle->SetMovePos(D3DXVECTOR3(FloatRandam(5.0f, -5.0f), FloatRandam(3.0f, 0.0f), FloatRandam(2.0f, -1.0f)));
		particle->SetMoveSize(D3DXVECTOR3(-0.35f, -0.35f, 0.0f));
		particle->SetMoveRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
		particle->SetTexture(CTexture::TEXTURE_SNOW);
	}

	for (int nCnt = 0; nCnt < MAX_PARTS; nCnt++)
	{
		if (m_pModel[nCnt] == nullptr)
		{// 影の高さ合わせ
			continue;
		}

		m_pModel[nCnt]->SetShadowHeight(m_meshHeigh - 1.0f);
	}
}

//--------------------------------------------------
// モーション
//--------------------------------------------------
void CPlayer3D::Motion_()
{
	for (int nCnt = 0; nCnt < MAX_PARTS; nCnt++)
	{
		if (m_pModel[nCnt] == nullptr)
		{// モデルに情報がなければ
			continue;
		}

		int nNextNumber = m_nCurrentKey + 1;

		if (nNextNumber >= m_ModelData[0].NUM_KEY)
		{
			nNextNumber = 0;
		}

		// 差分
		D3DXVECTOR3 difPos = m_ModelData[0].KeySet[nNextNumber].aKey[nCnt].pos - m_ModelData[0].KeySet[m_nCurrentKey].aKey[nCnt].pos;
		D3DXVECTOR3 difRot = m_ModelData[0].KeySet[nNextNumber].aKey[nCnt].rot - m_ModelData[0].KeySet[m_nCurrentKey].aKey[nCnt].rot;

		// 相対値
		float nRelativeValue = (float)m_nCountMotion / (float)m_ModelData[0].KeySet[m_nCurrentKey].nFrame;

		// 現在地
		D3DXVECTOR3 currentPos = m_ModelData[0].KeySet[m_nCurrentKey].aKey[nCnt].pos + (difPos * nRelativeValue);
		D3DXVECTOR3 currentRot = m_ModelData[0].KeySet[m_nCurrentKey].aKey[nCnt].rot + (difRot * nRelativeValue);

		m_pModel[nCnt]->SetPos(currentPos);
		m_pModel[nCnt]->SetRot(currentRot);
	}

	// モーションカウンターを進める
	m_nCountMotion++;

	if (m_nCountMotion >= m_ModelData[0].KeySet[m_nCurrentKey].nFrame)
	{
		m_nCurrentKey++;
		m_nCountMotion = 0;

		if (m_nCurrentKey >= m_nNumKey)
		{
			m_nCurrentKey = 0;
		}
	}
}

//---------------------------------------------------------------------------
// ファイル読み込み処理
//---------------------------------------------------------------------------
void CPlayer3D::LoadSetFile(char *Filename)
{
	m_nSetCurrentMotion = 0;
	char modelFile[256];
	char String[256];
	// ファイルポインタの宣言
	FILE* pFile;

	// ファイルを開く
	pFile = fopen(Filename, "r");

	if (pFile != NULL)
	{// ファイルが開いた場合
		fscanf(pFile, "%s", &String);

		while (strncmp(&String[0], "SCRIPT", 6) != 0)
		{// スタート来るまで空白読み込む
			String[0] = {};
			fscanf(pFile, "%s", &String[0]);
		}
		D3DXVECTOR3	s_modelMainpos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		while (strncmp(&String[0], "END_SCRIPT", 10) != 0)
		{// 文字列の初期化と読み込み// 文字列の初期化と読み込み
			fscanf(pFile, "%s", &String[0]);

			if (strcmp(&String[0], "MODEL_FILENAME") == 0)
			{// 文字列が一致した場合
				fscanf(pFile, "%s", &modelFile);
			}
			if (strcmp(&String[0], "MAINPOS") == 0)
			{// 文字列が一致した場合
				fscanf(pFile, "%s", &String[0]);		// ＝読み込むやつ
				fscanf(pFile, "%f", &s_modelMainpos.x);
				fscanf(pFile, "%f", &s_modelMainpos.y);
				fscanf(pFile, "%f", &s_modelMainpos.z);
			}
			else if ((strcmp(&String[0], "MODELSET") == 0) || (strcmp(&String[0], "MOTIONSET") == 0))
			{// 文字列が一致した場合
				D3DXVECTOR3	s_modelpos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
				D3DXVECTOR3	s_modelrot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

				while (1)
				{
					// 文字列の初期化と読み込み
					String[0] = {};
					fscanf(pFile, "%s", &String[0]);

					if (strncmp(&String[0], "#", 1) == 0)
					{// これのあとコメント
						fgets(&String[0], sizeof(String), pFile);
						continue;
					}

					if (strcmp(&String[0], "POS") == 0)
					{// 文字列が一致した場合
						fscanf(pFile, "%s", &String[0]);		// ＝読み込むやつ
						fscanf(pFile, "%f", &s_modelpos.x);
						fscanf(pFile, "%f", &s_modelpos.y);
						fscanf(pFile, "%f", &s_modelpos.z);
					}

					if (strcmp(&String[0], "ROT") == 0)
					{// 文字列が一致した場合
						fscanf(pFile, "%s", &String[0]);		// ＝読み込むやつ
						fscanf(pFile, "%f", &s_modelrot.x);
						fscanf(pFile, "%f", &s_modelrot.y);
						fscanf(pFile, "%f", &s_modelrot.z);
					}
				
					if (strcmp(&String[0], "LOOP") == 0)
					{// 文字列が一致した場合//ループするかしないか1する０しない
						fscanf(pFile, "%s", &String[0]);		// ＝読み込むやつ
						fscanf(pFile, "%d", &m_ModelData[m_nSetCurrentMotion].LOOP);
					}

					if (strcmp(&String[0], "NUM_KEY") == 0)
					{// 文字列が一致した場合//キーの最大数
						fscanf(pFile, "%s", &String[0]);		// ＝読み込むやつ
						fscanf(pFile, "%d", &m_ModelData[m_nSetCurrentMotion].NUM_KEY);
					}

					if (strcmp(&String[0], "KEYSET") == 0)
					{// 文字列が一致した場合//アニメーションのファイルだったとき
						LoadKeySetFile(pFile);
					}

					if (strcmp(&String[0], "END_MOTIONSET") == 0)
					{// 一回のmotion読み込み切ったら次のmotionのセットに行くためにカウント初期化してデータを加算する
						m_nSetModel = 0;
						m_nSetCurrentMotion++;
					}
	
					if (strcmp(&String[0], "END_SCRIPT") == 0)
					{// 文字列が一致した場合
						break;
					}
				}
			}
		}
	}

	//ファイルを閉じる
	fclose(pFile);
}

//---------------------------------------------------------------------------
// ファイル読み込み処理
//---------------------------------------------------------------------------
void CPlayer3D::LoadKeySetFile(FILE * pFile)
{
	char String[256];
	int nSetKey = 0;

	while (1)
	{
		// 文字列の初期化と読み込み
		fscanf(pFile, "%s", &String[0]);

		if (strncmp(&String[0], "#", 1) == 0)
		{//コメント対策
			fgets(&String[0], sizeof(String), pFile);
			continue;
		}

		if (strcmp(&String[0], "FRAME") == 0)
		{// 文字列が一致した場合
			fscanf(pFile, "%s", &String[0]);	// イコール読み込む
			fscanf(pFile, "%d", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].nFrame);
		}

		if (strcmp(&String[0], "KEY") == 0)
		{
			while (1)
			{// 文字列の初期化と読み込み
				String[0] = {};
				fscanf(pFile, "%s", &String[0]);
				if (strncmp(&String[0], "#", 1) == 0)
				{
					fgets(&String[0], sizeof(String), pFile);		// コメント読み込む
					continue;
				}

				if (strcmp(&String[0], "POS") == 0)
				{// 文字列が一致した場合
					fscanf(pFile, "%s", &String[0]);	//イコール読み込む
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].pos.x);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].pos.y);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].pos.z);
				}

				if (strcmp(&String[0], "ROT") == 0)
				{// 文字列が一致した場合
					fscanf(pFile, "%s", &String[0]);	//イコール読み込む
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].rot.x);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].rot.y);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].rot.z);
				}

				if (strcmp(&String[0], "END_KEY") == 0)
				{
					nSetKey++;//パーツの数がどんどん増える
					break;
				}
			}
		}

		if (strcmp(&String[0], "END_KEYSET") == 0)
		{// 文字列が一致した場合
			m_nSetModel++;		// 現在のセットしてる番号の更新
			nSetKey = 0;		// パーツの数初期化
			break;
		}
	}
}

//--------------------------------------------------
// アイテムとの当たり判定
//--------------------------------------------------
void CPlayer3D::CollisionItem_()
{
	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 size = GetSize();

	CObject *pObject = CObject::GetTop(PRIORITY_OBJECT);

	while (pObject)
	{// オブジェクトがnullptrになるまで
		CObject::TYPE_3D objType = pObject->GetType();
		if (objType == CObject::TYPE_COIN)
		{// そのオブジェクトがアイテムじゃなかったら
		 // ダイナミックキャスト
			CItem* pItem = dynamic_cast<CItem*>(pObject);
			if ((pItem == nullptr))
			{
				assert(false);
				return;
			}

			D3DXVECTOR3 targetPos = pItem->GetPos();
			D3DXVECTOR3 targetSize = pItem->GetSize();

			bool bIsLanding = IsCollisionSphere(targetPos, pos, targetSize, size);
			if (bIsLanding)
			{// 当たった時
			 // サウンド
				CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_GET_ITEM);

				for (int nCntParticle = 0; nCntParticle < COIN_PARTICLE; nCntParticle++)
				{// アイテム取得時のパーティクル
					CParticle* particle = CParticle::Create(D3DXVECTOR3(targetPos.x, targetPos.y + 50.0f, targetPos.z), D3DXVECTOR3(25.0f, 25.0f, 0.0f), 50);
					particle->SetMovePos(D3DXVECTOR3(FloatRandam(5.0f, -5.0f), FloatRandam(5.0f, 0.0f), 0.0f));
					particle->SetMoveSize(D3DXVECTOR3(-0.35f, -0.35f, 0.0f));
					particle->SetMoveRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
					particle->SetCol(D3DXCOLOR(1.0f, 0.8f, 0.0f, 1.0f));
					particle->SetTexture(CTexture::TEXTURE_COIN);
				}

				CGame::GetGame()->GetScore()->AddScore(100);
				pItem->Uninit();
			}
		}

		pObject = pObject->GetNext();
	}
}

//--------------------------------------------------
// 障害物との当たり判定
//--------------------------------------------------
bool CPlayer3D::CollisionObstacle_()
{
	bool bIsLanding = false;

	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 size = GetSize();

	CObject *pObject = CObject::GetTop(PRIORITY_OBJECT);

	while (pObject)
	{// オブジェクトがnullptrになるまで
		CObject::TYPE_3D objType = pObject->GetType();
		if (objType == CObject::TYPE_OBSTACLE)
		{// そのオブジェクトがアイテムじゃなかったら
		 // ダイナミックキャスト
			CObstacle* pObstacle = dynamic_cast<CObstacle*>(pObject);
			if ((pObstacle == nullptr))
			{
				assert(false);
				return bIsLanding;
			}

			D3DXVECTOR3 targetPos = pObstacle->GetPos();
			D3DXVECTOR3 targetSize = pObstacle->GetSize();

			bool bIsHit = IsCollision(targetPos, pos, targetSize, size);
			if (bIsHit)
			{// 当たった時
			 // サウンド
			 //CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_GET_ITEM);
				bIsLanding = true;
			}
		}

		pObject = pObject->GetNext();
	}

	return bIsLanding;
}

//--------------------------------------------------
// メッシュとの当たり判定
//--------------------------------------------------
void CPlayer3D::CollisionMesh_()
{
	// 法線の方向
	D3DXVECTOR3 meshNor(0.0f, 0.0f, 0.0f);
	// 重力
	D3DXVECTOR3 gravity(0.0f, -GRAVITY, 0.0f);

	m_bIsMesh = false;
	int maxMesh = CGame::GetGame()->GetMeshIndex();

	for (int nCnt = 0; nCnt < maxMesh; nCnt++)
	{// メッシュとの当たり判定
		CMeshField *pMeshField = CGame::GetGame()->GetMeshField(nCnt);
		bool bIsLanding = false;
		// 今いるメッシュの高さより下にいる場合
		bIsLanding = pMeshField->CMeshField::CollisionMesh(&m_pos, &m_posOld, &m_fMeahAngle, &m_fMeshMaxAngle, &meshNor, &m_meshHeigh);

		if (bIsLanding && !m_bIsHitObstacle)
		{
			m_bIsMesh = true;

			D3DXVECTOR3 move = m_pos - m_posOld;
			D3DXVec3Normalize(&move, &move);

			move *= D3DXVec3Length(&m_move);

			// 進行方向の計算
			D3DXVec3Normalize(&gravity, &gravity);
			D3DXVECTOR3 newVec = (D3DXVECTOR3(meshNor.x, meshNor.y, meshNor.z)) + gravity;
			D3DXVec3Normalize(&newVec, &newVec);

			move += newVec * m_accel;
			m_move = move;

			// インプット
			CInput *pInput = CInput::GetKey();

			if (pInput->Trigger(DIK_SPACE) || pInput->Trigger(JOYPAD_A, 0))
			{// 上
				m_move += (D3DXVECTOR3(0, 1, 0) + meshNor) * 10.0f;
			}

			if (m_move.y < -30.0f)
			{
				m_move.y = -30.0f;
			}
		}
	}
}

//--------------------------------------------------
// ゴールとの当たり判定
//--------------------------------------------------
void CPlayer3D::CollisionGoal_()
{
	CGoal *pGoal = CGame::GetGame()->GetGoal();
	// 矩形の当たり判定
	bool isHit = pGoal->CollisionGoal(&m_pos, &m_posOld, &m_size);
	if (isHit)
	{// ゴール後の動き
		m_nGoalTimer++;

		// ゴールした時のUI
		CObject2D::Create(
			D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, CManager::SCREEN_HEIGHT * 0.5f, 0.0f),
			D3DXVECTOR3(500.0f, 200.0f, 0.0f), PRIORITY_UI)->SetTexture(CTexture::TEXTURE_GOAL);

		if (m_nGoalTimer > 40)
		{// 一定時間
			m_bIslandingGoal = true;
			// ゴールした時の音
			//CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_GOAL);
		}
	}
}
