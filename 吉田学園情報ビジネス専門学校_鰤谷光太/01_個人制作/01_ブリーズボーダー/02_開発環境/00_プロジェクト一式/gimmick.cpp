//==================================================
// gimmick.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "utility.h"

#include "gimmick.h"
#include "objectX.h"
#include "model.h"
#include "manager.h"
#include "model_data.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CGimmick::CGimmick(int nPriority) : CObjectX(nPriority)
{
	SetModelData(CModelData::MODEL_GIMMICK);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CGimmick::~CGimmick()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CGimmick::Init()
{
	CObjectX::Init();

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CGimmick::Uninit()
{
	CObjectX::Uninit();
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CGimmick::Draw(DRAW_MODE drawMode)
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetManager()->GetRenderer()->GetDevice();

	CObjectX::Draw(drawMode);

	if (m_isStencil)
	{
		// ステンシルバッファ -> 有効
		pDevice->SetRenderState(D3DRS_STENCILENABLE, TRUE);

		// ステンシルバッファと比較する参照値設定 -> ref
		pDevice->SetRenderState(D3DRS_STENCILREF, 0x01);

		// ステンシルバッファの値に対してのマスク設定 -> 0xff(全て真)
		pDevice->SetRenderState(D3DRS_STENCILMASK, 0xff);

		// ステンシルテストの比較方法 ->
		// （参照値 >= ステンシルバッファの参照値）なら合格
		pDevice->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_EQUAL);

		// ステンシルテストの結果に対しての反映設定
		pDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_INCR);			// Zとステンシル成功
		pDevice->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);			// Zとステンシル失敗
		pDevice->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);			// Zのみ失敗
	}
	else
	{
		// ステンシルバッファ -> 無効
		pDevice->SetRenderState(D3DRS_STENCILENABLE, FALSE);
	}

	CObjectX::Draw(drawMode);

	// ステンシルバッファ -> 無効
	pDevice->SetRenderState(D3DRS_STENCILENABLE, FALSE);
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CGimmick *CGimmick::Create()
{
	CGimmick *pGimmick;
	pGimmick = new CGimmick;

	if (pGimmick != nullptr)
	{
		pGimmick->Init();
	}
	else
	{
		assert(false);
	}

	return pGimmick;
}

//--------------------------------------------------
// 当たり判定(線分)
//--------------------------------------------------
bool CGimmick::CollisionGimmick(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pPosOld, D3DXVECTOR3 *pSize)
{
	CModelData* modelData = CManager::GetManager()->GetModelData();

	D3DXMATRIX mtxWorld = CObjectX::GetMtxWorld();

	bool bIsLanding = false;

	D3DXVECTOR3 min = modelData->GetModel(GetModelData()).vtxMin;
	D3DXVECTOR3 max = modelData->GetModel(GetModelData()).vtxMax;

	// 座標を入れる箱
	D3DXVECTOR3 localPos[4];
	D3DXVECTOR3 worldPos[4];

	// ローカルの座標
	localPos[0] = D3DXVECTOR3(min.x, 0.0f, max.z);
	localPos[1] = D3DXVECTOR3(max.x, 0.0f, max.z);
	localPos[2] = D3DXVECTOR3(max.x, 0.0f, min.z);
	localPos[3] = D3DXVECTOR3(min.x, 0.0f, min.z);

	for (int nCnt = 0; nCnt < 4; nCnt++)
	{// ローカルからワールドに変換
		D3DXVec3TransformCoord(&worldPos[nCnt], &localPos[nCnt], &mtxWorld);

		D3DXVECTOR3 vec = worldPos[nCnt];
		D3DXVec3Normalize(&vec, &vec);
		// 大きめにとる
		worldPos[nCnt] += (vec * 50.0f);
	}

	D3DXVECTOR3 vecPlayer[4];

	// 頂点座標の取得
	vecPlayer[0] = *pPos - worldPos[0];
	vecPlayer[1] = *pPos - worldPos[1];
	vecPlayer[2] = *pPos - worldPos[2];
	vecPlayer[3] = *pPos - worldPos[3];

	D3DXVECTOR3 vecLine[4];

	// 四辺の取得 (v2)
	vecLine[0] = worldPos[1] - worldPos[0];
	vecLine[1] = worldPos[2] - worldPos[1];
	vecLine[2] = worldPos[3] - worldPos[2];
	vecLine[3] = worldPos[0] - worldPos[3];

	float InOut[4];

	InOut[0] = Vec2Cross(&vecLine[0], &vecPlayer[0]);
	InOut[1] = Vec2Cross(&vecLine[1], &vecPlayer[1]);
	InOut[2] = Vec2Cross(&vecLine[2], &vecPlayer[2]);
	InOut[3] = Vec2Cross(&vecLine[3], &vecPlayer[3]);

	D3DXVECTOR3 pos = GetPos();

	// Yの押出
	if (InOut[0] < 0.0f && InOut[1] < 0.0f && InOut[2] < 0.0f && InOut[3] < 0.0f)
	{
		if (pPosOld->y >= pos.y + max.y && pPos->y < pos.y + max.y)
		{// 上
			pPos->y = pos.y + max.y;
			bIsLanding = true;
		}
	}

	if (pPos->y < pos.y + max.y && pPos->y + pSize->y > pos.y)
	{// XZの押出
		for (int nCnt = 0; nCnt < 4; nCnt++)
		{// どの方向から来たか判定
			D3DXVECTOR3 vecPosOld = *pPosOld - worldPos[nCnt];
			float leftPosOld = Vec2Cross(&vecLine[nCnt], &vecPosOld);

			if (leftPosOld >= 0.0f)
			{// 方向が分かった時
				bIsLanding = true;

				// posOldから始点までの距離（V）
				D3DXVECTOR3 V = worldPos[nCnt] - *pPosOld;
				// プレイヤーのMOVE（V1）
				D3DXVECTOR3 vecMove = *pPos - *pPosOld;

				float t1 = Vec2Cross(&V, &vecLine[nCnt]) / Vec2Cross(&vecMove, &vecLine[nCnt]);
				float t2 = Vec2Cross(&V, &vecMove) / Vec2Cross(&vecMove, &vecLine[nCnt]);

				const float eps = 0.00001f;
				if (t1 + eps < 0 || t1 - eps > 1 || t2 + eps < 0 || t2 - eps > 1)
				{// 交差していない
					continue;
				}
				else
				{// 押し出し用法線の格納用の箱
					D3DXVECTOR3 nor;
					// 上方向のベクトル
					D3DXVECTOR3 vecUp(0.0f, 1.0f, 0.0f);
					// 正規化
					D3DXVec3Normalize(&vecLine[nCnt], &vecLine[nCnt]);
					// メッシュの法線を求める
					D3DXVec3Cross(&nor, &vecLine[nCnt], &vecUp);
					// 大きさを１にする
					D3DXVec3Normalize(&nor, &nor);

					// 逆方向
					D3DXVECTOR3 reverseVecMove = *pPosOld - *pPos;
					// (a)
					float difMove = D3DXVec2Dot(&reverseVecMove, &nor);

					// 押し返し
					pPos->x = (pPosOld->x + vecMove.x * t1) + (nor.x * 0.1f) + (vecMove.x + difMove * nor.x);
					pPos->z = (pPosOld->z + vecMove.z * t1) + (nor.z * 0.1f) + (vecMove.z + difMove * nor.z);

					break;
				}
			}
		}
	}
	return bIsLanding;
}
