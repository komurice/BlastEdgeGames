//==================================================
// item.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "manager.h"
#include "game.h"

#include "player3D.h"
#include "item.h"
#include "item_coin.h"

//**************************************************
// 静的メンバ変数
//**************************************************
CItem::ITEM_TYPE CItem::m_itemType;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CItem::CItem(int nPriority) : CGimmick(nPriority)
{
	// タイプの設定
	SetType(TYPE_ITEM);
	SetStencil(true);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CItem::~CItem()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CItem::Init()
{
	CObjectX::Init();

	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CItem::Update()
{
	CObjectX::Update();
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CItem::Draw(DRAW_MODE drawMode)
{
	if (drawMode == DRAW_MODE_SHADOW)
	{
		if (CGame::GetGame()->GetPlayer3D() != nullptr)
		{
			D3DXVECTOR3 diff = GetPos() - CGame::GetGame()->GetPlayer3D()->GetPos();

			if (D3DXVec3LengthSq(&diff) < 7000.0f * 7000.0f)
			{
				CObjectX::Draw(drawMode);
			}
		}
	}
	else
	{
		CObjectX::Draw(drawMode);
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CItem *CItem::Create(ITEM_TYPE type)
{
	CItem *pItem = nullptr;
	m_itemType = type;

	// 現在の画面(モード)の終了処理
	switch (type)
	{
	case ITEM_TYPE_COIN:
		pItem = CCoin::Create();

		break;

	default:
		assert(false);
		break;
	}

	return pItem;
}
