//==================================================
// obstacle.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "manager.h"
#include "game.h"
#include "debug_proc.h"
#include "obstacle.h"

#include "model_data.h"
#include "player3D.h"

//**************************************************
// 静的メンバ変数
//**************************************************
CObstacle::OBSTACLE_MODEL_TYPE CObstacle::m_ObstacleModelType;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CObstacle::CObstacle(int nPriority) : CGimmick(nPriority)
{
	// タイプの設定
	SetType(TYPE_OBSTACLE);
}

//--------------------------------------------------
//	デストラクタ
//--------------------------------------------------
CObstacle::~CObstacle()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CObstacle::Init()
{
	CObjectX::Init();

	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CObstacle::Update()
{
	CObjectX::Update();
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CObstacle::Draw(DRAW_MODE drawMode)
{
	if (drawMode == DRAW_MODE_SHADOW)
	{
		if (CGame::GetGame()->GetPlayer3D() != nullptr)
		{
			D3DXVECTOR3 diff = GetPos() - CGame::GetGame()->GetPlayer3D()->GetPos();

			if (D3DXVec3LengthSq(&diff) < 7000.0f * 7000.0f)
			{
				CObjectX::Draw(drawMode);
			}
		}
	}
	else
	{
		CObjectX::Draw(drawMode);
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CObstacle *CObstacle::Create(OBSTACLE_MODEL_TYPE type)
{
	CObstacle *pObstacle = nullptr;
	pObstacle = new CObstacle;
	m_ObstacleModelType = type;

	pObstacle->Init();

	CModelData* modelData = CManager::GetManager()->GetModelData();

	switch (type)
	{
	case OBSTACLE_MODEL_TYPE_TREE:
		pObstacle->SetModelData(CModelData::MODEL_TREE);
		pObstacle->SetSize(modelData->GetModel(CModelData::MODEL_TREE).size);
		pObstacle->SetStencil(true);

		break;

	case OBSTACLE_MODEL_TYPE_CLEENNESS_OBJECT:
		pObstacle->SetModelData(CModelData::MODEL_CLEENNESS_OBJECT);
		pObstacle->SetSize(modelData->GetModel(CModelData::MODEL_CLEENNESS_OBJECT).size);
		pObstacle->SetStencil(false);

		break;

	case OBSTACLE_MODEL_TYPE_CLEENNESS_OBJECT_VERTICAL:
		pObstacle->SetModelData(CModelData::MODEL_CLEENNESS_OBJECT_VERTICAL);
		pObstacle->SetSize(modelData->GetModel(CModelData::MODEL_CLEENNESS_OBJECT_VERTICAL).size);
		pObstacle->SetStencil(false);

		break;

	case OBSTACLE_MODEL_TYPE_CLEENNESS_OBJECT_SHORT:
		pObstacle->SetModelData(CModelData::MODEL_CLEENNESS_OBJECT_SHORT);
		pObstacle->SetSize(modelData->GetModel(CModelData::MODEL_CLEENNESS_OBJECT_SHORT).size);
		pObstacle->SetStencil(false);

		break;

	case OBSTACLE_MODEL_TYPE_CLEENNESS_OBJECT_VERTICAL_SHORT:
		pObstacle->SetModelData(CModelData::MODEL_CLEENNESS_OBJECT_VERTICAL_SHORT);
		pObstacle->SetSize(modelData->GetModel(CModelData::MODEL_CLEENNESS_OBJECT_VERTICAL_SHORT).size);
		pObstacle->SetStencil(false);

		break;

	default:
		assert(false);
		break;
	}

	return pObstacle;
}
