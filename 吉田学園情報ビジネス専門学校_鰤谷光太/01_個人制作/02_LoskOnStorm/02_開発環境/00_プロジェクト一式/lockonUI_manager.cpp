//==================================================
// lockonUI.h
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "lockonUI.h"
#include "lockonUI_manager.h"

#include "lock_on_cursor.h"

//**************************************************
// 静的メンバ変数
//**************************************************
CLockOnUI *CLockOnUIManager::m_pLockOnUI[MAX_UI] = {};
int CLockOnUIManager::m_nMaxLockOn = 0;
int CLockOnUIManager::m_nNowLockOn = 0;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CLockOnUIManager::CLockOnUIManager(int nPriority /* =4 */) : CObject(nPriority)
{
	m_nMaxLockOn = 3;
	SetType(TYPE_LOCKON_UI);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CLockOnUIManager::~CLockOnUIManager()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CLockOnUIManager::Init()
{
	for (int nCnt = 0; nCnt < MAX_UI; nCnt++)
	{
		m_pLockOnUI[nCnt] = CLockOnUI::Create(D3DXVECTOR3(100.0f - nCnt * 2.5f, 50.0f * nCnt + 200.0f, 0.0f), D3DXVECTOR3(100.0f - nCnt * 5.0f, 40.0f, 0.0f));

		if (nCnt >= 3)
		{
			m_pLockOnUI[nCnt]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
		}
	}

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CLockOnUIManager::Uninit()
{
	for (int nCnt = 0; nCnt < MAX_UI; nCnt++)
	{
		if (m_pLockOnUI[nCnt] == nullptr)
		{
			continue;
		}

		m_pLockOnUI[nCnt]->Uninit();
	}

	CObject::Release();
}

//--------------------------------------------------
// 加算
//--------------------------------------------------
void CLockOnUIManager::AddUI(int nValue)
{
	m_nMaxLockOn += nValue;

	if (m_nMaxLockOn >= 8)
	{
		m_nMaxLockOn = 8;
	}

	for (int nCnt = 0; nCnt < m_nMaxLockOn; nCnt++)
	{
		m_pLockOnUI[nCnt]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}
}

//--------------------------------------------------
// ロックオンした数に応じて色を薄くする
//--------------------------------------------------
void CLockOnUIManager::LockOnNum(int nValue)
{
	m_nNowLockOn = nValue;

	for (int nCnt = 0; nCnt < m_nNowLockOn; nCnt++)
	{
		m_pLockOnUI[nCnt]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f));
	}
}

//--------------------------------------------------
// ロックオンした数に応じて色を元に戻す
//--------------------------------------------------
void CLockOnUIManager::ClearLockOn(int nValue)
{
	m_nNowLockOn = nValue;

	for (int nCnt = 0; nCnt < m_nNowLockOn; nCnt++)
	{
		m_pLockOnUI[nCnt]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f));
	}

	for (int nCnt = m_nNowLockOn; nCnt < m_nMaxLockOn; nCnt++)
	{
		m_pLockOnUI[nCnt]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CLockOnUIManager *CLockOnUIManager::Create()
{
	CLockOnUIManager *pLockOnUIManager;
	pLockOnUIManager = new CLockOnUIManager;
	
	if (pLockOnUIManager != nullptr)
	{
		pLockOnUIManager->Init();
	}
	else
	{
		assert(false);
	}

	return pLockOnUIManager;
}
