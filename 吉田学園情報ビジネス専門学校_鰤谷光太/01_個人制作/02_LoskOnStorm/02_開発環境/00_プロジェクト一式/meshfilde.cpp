//==================================================
// meshfilde.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include "manager.h"

#include "meshfilde.h"

//**************************************************
// マクロ定義
//**************************************************
#define MESHFILDE_SIZE			(80.0f)
#define HEIGHT					(57.0f)
#define DIFF_HEIGHT				(24.0f)
#define SHIFT_X					(5.0f)
#define SHIFT_Z					(35.0f)
#define MESH_X					(60)
#define MESH_Z					(45)
#define MESH_LENGTH				(MESHFILDE_SIZE * MESH_Z)
#define MESH_VTX				(((MESH_X) + 1) * ((MESH_Z) + 1))
#define MESH_IDX				((((MESH_X) + 1) * 2) * ((MESH_Z) * ((MESH_Z) - 1)) * (MESH_Z) * 2)
#define MESH_PRIMITIVE			((MESH_X) * (MESH_Z) * 2 + 4 * ((MESH_Z)  - 1))

//**************************************************
// 定数定義
//**************************************************
// 頂点フォーマット
const DWORD FVF_VERTEX_3D = (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1);		// 座標・法線・カラー・テクスチャ

//**************************************************
// 構造体定義
//**************************************************
// 頂点の情報[3D]の構造体を定義
struct VERTEX_3D
{
	D3DXVECTOR3 pos;	// 頂点座標	
	D3DXVECTOR3 nor;	// 法線ベクトル
	D3DCOLOR col;		// 頂点カラー	
	D3DXVECTOR2 tex;	// テクスチャの座標
};

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CMeshFilde::CMeshFilde(int nPriority /* =1 */) : CObject(nPriority)
{
	// タイプ設定
	SetType(TYPE_ROAD);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CMeshFilde::~CMeshFilde()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CMeshFilde::Init()
{
	// デバイスのポインタ
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

	// テクスチャの設定
	CMeshFilde::SetTexture(CTexture::TEXTURE_ROAD);

	// 頂点バッファの生成
	pDevice->CreateVertexBuffer(sizeof(VERTEX_3D) * MESH_VTX,
		D3DUSAGE_WRITEONLY,
		FVF_VERTEX_3D,
		D3DPOOL_MANAGED,
		&m_pVtxBuff,
		NULL);

	//インデックスバッファの生成
	pDevice->CreateIndexBuffer(sizeof(WORD) * MESH_IDX,
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&m_pIdxBuff,
		NULL);

	VERTEX_3D* pVtx = NULL;			// 頂点情報へのポインタ

	// 頂点バッファをロックし、頂点情報へのポインタを取得
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	for (int z = 0; z < (MESH_Z + 1); z++)
	{
		for (int x = 0; x < (MESH_X + 1); x++)
		{
			pVtx[x + z * (MESH_X + 1)].pos = D3DXVECTOR3(x * MESHFILDE_SIZE, 0.0f, -z * MESHFILDE_SIZE);
			pVtx[x + z * (MESH_X + 1)].pos.x -= MESH_X * MESHFILDE_SIZE * 0.5f - SHIFT_X;
			pVtx[x + z * (MESH_X + 1)].pos.z += MESH_Z * MESHFILDE_SIZE * 0.5f + SHIFT_Z;
			pVtx[x + z * (MESH_X + 1)].nor = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
			pVtx[x + z * (MESH_X + 1)].col = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
			pVtx[x + z * (MESH_X + 1)].tex = D3DXVECTOR2((float)1.0f / MESH_X * x, (float)1.0f / MESH_Z * z);
		}
	}

	WORD* pIdx;

	// インデックスバッファをロック
	m_pIdxBuff->Lock(0, 0, (void**)&pIdx, 0);

	for (int z = 0; z < MESH_Z; z++)
	{// zの方向
	 //
		int nLineTop = z * ((MESH_X + 1) * 2 + 2);
		for (int x = 0; x < MESH_X + 1; x++)
		{	// xの方向
			// 偶数番目
			pIdx[(x * 2) + nLineTop] = (WORD)((MESH_X + 1) + x + z * (MESH_X + 1));
			// 奇数番目
			pIdx[(x * 2 + 1) + nLineTop] = (WORD)(pIdx[(x * 2) + nLineTop] - (MESH_X + 1));
		}
		// 縮退ポリゴン(数が連続する時)
		if (z < MESH_Z - 1)
		{
			pIdx[(MESH_X + 1) * 2 + nLineTop] = (WORD)(MESH_X + (MESH_X + 1) * z);
			pIdx[(MESH_X + 1) * 2 + 1 + nLineTop] = (WORD)((MESH_X + 1) * (2 + z));
		}
	}

	// 三角形の頂点数
	const int nTri = 3;
	// 名前思いつかない
	D3DXVECTOR3 posCorner[nTri];
	// primitiveを保存
	int primitive = MESH_PRIMITIVE;

	// 頂点数分初期化
	for (int i = 0; i < MESH_VTX; i++)
	{
		pVtx[i].nor = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	}
	// 
	for (int nCnt = 0; nCnt < primitive; nCnt++)
	{
		posCorner[0] = pVtx[pIdx[nCnt + 0]].pos;
		posCorner[1] = pVtx[pIdx[nCnt + 1]].pos;
		posCorner[2] = pVtx[pIdx[nCnt + 2]].pos;

		if ((pIdx[nCnt + 0] == pIdx[nCnt + 1]) ||
			(pIdx[nCnt + 0] == pIdx[nCnt + 2]) ||
			(pIdx[nCnt + 2] == pIdx[nCnt + 1]))
		{
			continue;
		}

		D3DXVECTOR3 V1 = posCorner[1] - posCorner[0];
		D3DXVECTOR3 V2 = posCorner[2] - posCorner[0];

		D3DXVECTOR3 vecNormal;

		if (nCnt % 2 == 0)
		{
			// メッシュの法線を求める
			D3DXVec3Cross(&vecNormal, &V1, &V2);
		}
		else
		{
			// メッシュの法線を求める
			D3DXVec3Cross(&vecNormal, &V2, &V1);
		}

		// 大きさを１にする
		D3DXVec3Normalize(&vecNormal, &vecNormal);

		for (int i = 0; i < nTri; i++)
		{
			pVtx[pIdx[nCnt + i]].nor += vecNormal;
		}
	}

	for (int nCnt = 0; nCnt < MESH_VTX; nCnt++)
	{// 大きさを１にする
		D3DXVec3Normalize(&pVtx[nCnt].nor, &pVtx[nCnt].nor);
	}

	// 頂点バッファをアンロックする
	m_pVtxBuff->Unlock();

	// インデックスバッファのアンロック
	m_pIdxBuff->Unlock();

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CMeshFilde::Uninit()
{
	if (m_pVtxBuff != nullptr)
	{// 頂点バッファの破棄
		m_pVtxBuff->Release();
		m_pVtxBuff = nullptr;
	}

	if (m_pIdxBuff != nullptr)
	{// インデックスバッファの破棄
		m_pIdxBuff->Release();
		m_pIdxBuff = nullptr;
	}

	Release();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CMeshFilde::Update()
{
	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 move(0.0f, 0.0f, -3.0f);

	// posにmoveを加算
	pos += move;

	if (pos.z < -MESH_LENGTH + 150.0f)
	{
		pos.z += MESH_LENGTH * 2.0f;
	}

	// posを代入
	SetPos(pos);
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CMeshFilde::Draw()
{
	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();
	CTexture* pTexture = CManager::GetTexture();
	D3DXMATRIX mtxRot, mtxTrans;								//計算用マトリックス

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxWorld);

	// 向きを反映								↓rotの情報を使って回転行列を作る
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);		//行列掛け算関数		第二引数 * 第三引数 を　第一引数に格納

	// 位置を反映								↓posの情報を使って移動行列を作る
	D3DXMatrixTranslation(&mtxTrans, m_pos.x, m_pos.y, m_pos.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);

	// ワールドマトリックスの設定
	pDevice->SetTransform(D3DTS_WORLD, &m_mtxWorld);

	// 頂点バッファをデータストリームに設定
	pDevice->SetStreamSource(0, m_pVtxBuff, 0, sizeof(VERTEX_3D));

	// インデックスバッファをデータストリームに設定
	pDevice->SetIndices(m_pIdxBuff);

	// 頂点フォーマットの設定VERTEX_3D
	pDevice->SetFVF(FVF_VERTEX_3D);

	// テクスチャの設定
	pDevice->SetTexture(0, pTexture->GetTexture(m_texture));

	// ポリゴンの描画
	pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, MESH_VTX, 0, MESH_PRIMITIVE);

	// テクスチャの解除
	pDevice->SetTexture(0, NULL);
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CMeshFilde* CMeshFilde::Create()
{
	CMeshFilde *pMeshFilde;
	pMeshFilde = new CMeshFilde;

	if (pMeshFilde != nullptr)
	{
		pMeshFilde->Init();
	}
	else
	{
		assert(false);
	}

	return pMeshFilde;
}
