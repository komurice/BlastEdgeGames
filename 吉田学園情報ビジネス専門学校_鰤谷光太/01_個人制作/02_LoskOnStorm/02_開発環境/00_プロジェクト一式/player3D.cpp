//==================================================
// player3D.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "utility.h"

#include "manager.h"
#include "camera.h"
#include "input_keyboard.h"
#include "input_joypad.h"
#include "fade.h"

#include "polygon3D.h"
#include "lock_on_cursor.h"
#include "player3D.h"
#include "bullet.h"
#include "explosion.h"
#include "homing_laser.h"
#include "enemy3D.h"
#include "life_manager.h"
#include "add_lockon_item.h"
#include "lockonUI_manager.h"
#include "sound.h"

//**************************************************
// 静的メンバ変数
//**************************************************
const float CPlayer3D::PLAYER_SPEED = 15.0f;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CPlayer3D::CPlayer3D()
{
	// タイプの設定
	SetType(TYPE_PLAYER);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CPlayer3D::~CPlayer3D()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CPlayer3D::Init()
{
	// 当たり判定でとる大きさ
	CObject3D::SetSize(D3DXVECTOR3(50.0f, 50.0f, 50.0f));

	CObject3D::Init();

	// ロックオンの位置
	m_pLockOnCursor = CLockOnCursor::Create(D3DXVECTOR3(60.0f, 0.0f, 60.0f), PRIORITY_EFFECT);
	m_pLockOnCursor->SetPos(D3DXVECTOR3(0.0f, 300.0f, 200.0f));

	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CPlayer3D::Update()
{
	CObject3D::Update();

	// プレイヤーの動き
	Control_();

	// 当たり判定
	CollisionItem_();
	CollisionEnemy_();

	// 画面内に収める
	InScreen_();
}

//--------------------------------------------------
// 画面内の納める
//--------------------------------------------------
void CPlayer3D::InScreen_()
{
	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 posLockOnCursor = m_pLockOnCursor->GetPos();

	// 画面内に収める処理
	if (pos.z > 600.0f)
	{// 下
		pos.z = 600.0f;
		posLockOnCursor.z = 600.0f + 500.0f;
	}
	if (pos.z < -670.0f)
	{// 上
		pos.z = -670.0f;
		posLockOnCursor.z = -670.0f + 500.0f;
	}
	if (pos.x > 600.0f)
	{// 右
		pos.x = 600.0f;
		posLockOnCursor.x = 600.0f;
	}
	if (pos.x < -600.0f)
	{// 左
		pos.x = -600.0f;
		posLockOnCursor.x = -600.0f;
	}

	SetPos(pos);
	m_pLockOnCursor->SetPos(posLockOnCursor);
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CPlayer3D* CPlayer3D::Create()
{
	CPlayer3D *pPlayer3D = nullptr;
	pPlayer3D = new CPlayer3D;

	if (pPlayer3D != nullptr)
	{
		pPlayer3D->Init();
	}
	else
	{
		assert(false);
	}

	return pPlayer3D;
}

//--------------------------------------------------
// 操作
//--------------------------------------------------
void CPlayer3D::Control_()
{
	CInputKeyboard *pInputKeyoard = CManager::GetInputKeyboard();
	CInputJoyPad *pInputJoyPad = CManager::GetInputJoyPad();

	D3DXVECTOR3 posLockOn = m_pLockOnCursor->GetPos();

	D3DXVECTOR3 vec(0.0f, 0.0f, 0.0f);

	vec.x = pInputJoyPad->GetJoypadStick(pInputJoyPad->JOYKEY_LEFT_STICK, 0).x;
	vec.z = -pInputJoyPad->GetJoypadStick(pInputJoyPad->JOYKEY_LEFT_STICK, 0).y;

	if ((vec.x == 0.0f) && (vec.z == 0.0f))
	{
		if (pInputKeyoard->GetPress(DIK_A))
		{// 左
			vec.x += -1.0f;
		}
		if (pInputKeyoard->GetPress(DIK_D))
		{// 右
			vec.x += 1.0f;
		}
		if (pInputKeyoard->GetPress(DIK_W))
		{// 上
			vec.z += 1.0f;
		}
		if (pInputKeyoard->GetPress(DIK_S))
		{// 下
			vec.z += -1.0f;
		}
	}

	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 rot = GetRot();

	if (vec.x != 0.0f || vec.z != 0.0f)
	{// 移動とそれに合わせた回転
	 // ベクトルの正規化
		D3DXVec3Normalize(&vec, &vec);
		// キーボード
		pos += vec * PLAYER_SPEED;
		posLockOn += vec * PLAYER_SPEED;

		SetPos(pos);
		m_pLockOnCursor->SetPos(posLockOn);

		rot.z += D3DXToRadian(vec.x) * 5.0f;

		if (rot.z > D3DXToRadian(45.0f))
		{
			rot.z = D3DXToRadian(45.0f);
		}
		else if (rot.z < D3DXToRadian(-45.0f))
		{
			rot.z = D3DXToRadian(-45.0f);
		}

		SetRot(rot);
	}

	if (vec.x == 0.0f)
	{// 水平に戻す
		rot.z *= 0.8f;
		SetRot(rot);
	}

	if (pInputJoyPad->GetJoypadPress(pInputJoyPad->JOYKEY_A, 0) || pInputKeyoard->GetPress(DIK_SPACE))
	{
		m_time++;

		if (m_time == 15)
		{
			// サウンド
			CManager::GetSound()->Play(CSound::LABEL_SE_SHOT);

			CBullet::Create(D3DXVECTOR3(0.0f, 0.0f, 20.0f), D3DXVECTOR3(15.0f, 15.0f, 0.0f))->SetPos(pos + D3DXVECTOR3(0.0f, 0.0f, 90.0f));
			CBullet::Create(D3DXVECTOR3(0.0f, 0.0f, 20.0f), D3DXVECTOR3(15.0f, 15.0f, 0.0f))->SetPos(pos + D3DXVECTOR3(60.0f, -15.0f, 60.0f));
			CBullet::Create(D3DXVECTOR3(0.0f, 0.0f, 20.0f), D3DXVECTOR3(15.0f, 15.0f, 0.0f))->SetPos(pos + D3DXVECTOR3(60.0f, 15.0f, 60.0f));
			CBullet::Create(D3DXVECTOR3(0.0f, 0.0f, 20.0f), D3DXVECTOR3(15.0f, 15.0f, 0.0f))->SetPos(pos + D3DXVECTOR3(-60.0f, -15.0f, 60.0f));
			CBullet::Create(D3DXVECTOR3(0.0f, 0.0f, 20.0f), D3DXVECTOR3(15.0f, 15.0f, 0.0f))->SetPos(pos + D3DXVECTOR3(-60.0f, 15.0f, 60.0f));

			m_time = 0;
		}
	}

	if (pInputJoyPad->GetJoypadTrigger(pInputJoyPad->JOYKEY_X, 0) || pInputKeyoard->GetTrigger(DIK_RETURN))
	{
		// サウンド
		CManager::GetSound()->Play(CSound::LABEL_SE_LASER);

		CEnemy3D **pEnemy = m_pLockOnCursor->GetLockOnTarget();

		int nNumLockOn = m_pLockOnCursor->GetNumLockOn();
		float Angle = 180.0f / (nNumLockOn + 1);

		for (int nCnt = 0; nCnt < MAX_TARGET; nCnt++)
		{
			if (pEnemy[nCnt] != nullptr)
			{// 発射の向き
				D3DXVECTOR3 shotDirection(40.0f * sinf(D3DXToRadian((Angle - 90.0f) * nCnt)), 0.0f, -40.0f);

				CHomingLaser::Create(D3DXVECTOR3(15.0f, 15.0f, 0.0f), shotDirection, pEnemy[nCnt])->SetPos(pos);
			}
		}
	}
}

//--------------------------------------------------
// エネミーとの当たり判定
//--------------------------------------------------
void CPlayer3D::CollisionEnemy_()
{
	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 size = GetSize();

	//CObject::ExecCObjectAll([this]{
	//	//処理
	//});
	for (int nCnt = 0; nCnt < CObject::MAX_OBjECT; nCnt++)
	{
		CObject **pObject = CObject::GetCObject(PRIORITY_OBJECT);
		if (pObject[nCnt] == nullptr)
		{
			continue;
		}

		CObject::TYPE_3D objType = pObject[nCnt]->GetType();
		if (objType != CObject::TYPE_ENEMY)
		{// 球の当たり判定
			continue;
		}

		// ダイナミックキャスト
		CEnemy3D* pEnemy = dynamic_cast<CEnemy3D*>(pObject[nCnt]);
		if ((pEnemy == nullptr))
		{
			assert(false);
			continue;
		}

		D3DXVECTOR3 targetPos = pEnemy->GetPos();
		D3DXVECTOR3 targetSize = pEnemy->GetSize();

		bool bIsLanding = IsCollisionSphere(targetPos, pos, targetSize, size);
		if (bIsLanding)
		{// 当たった時
			// サウンド
			CManager::GetSound()->Play(CSound::LABEL_SE_HIT);

			CExplosion::Create(D3DXVECTOR3(60.0f, 60.0f, 0.0f))->SetPos(pEnemy->GetPos());
			CExplosion::Create(D3DXVECTOR3(60.0f, 60.0f, 0.0f))->SetPos(this->GetPos());
			pObject[nCnt]->Uninit();

			CLifeManager::SubLife(1);

			if (CLifeManager::GetLife() == 0)
			{
				m_pLockOnCursor->Uninit();
				Uninit();

				CFade::GetInstance()->SetFade(CManager::MODE_RANKING);
			}
		}
	}
}

//--------------------------------------------------
// アイテムとの当たり判定
//--------------------------------------------------
void CPlayer3D::CollisionItem_()
{
	CObject3D::Update();

	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 size = GetSize();

	for (int nCnt = 0; nCnt < CObject::MAX_OBjECT; nCnt++)
	{
		CObject **pObject = CObject::GetCObject(PRIORITY_OBJECT);
		if (pObject[nCnt] == nullptr)
		{
			continue;
		}

		CObject::TYPE_3D objType = pObject[nCnt]->GetType();
		if (objType != CObject::TYPE_ITEM)
		{// 球の当たり判定
			continue;
		}

		// ダイナミックキャスト
		CAddLockOnItem* pItem = dynamic_cast<CAddLockOnItem*>(pObject[nCnt]);
		if ((pItem == nullptr))
		{
			assert(false);
			continue;
		}

		D3DXVECTOR3 targetPos = pItem->GetPos();
		D3DXVECTOR3 targetSize = pItem->GetSize();

		bool bIsLanding = IsCollisionSphere(targetPos, pos, targetSize, size);
		if (bIsLanding)
		{// 当たった時
			// サウンド
			CManager::GetSound()->Play(CSound::LABEL_SE_ITEM);

			m_pLockOnCursor->AddLockOn();
			CLockOnUIManager::AddUI(1);

			pItem->Uninit();
		}
	}
}
