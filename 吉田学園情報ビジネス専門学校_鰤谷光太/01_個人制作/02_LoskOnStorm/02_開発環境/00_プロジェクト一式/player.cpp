//==================================================
// player.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "player.h"
#include "manager.h"
#include "input_keyboard.h"
#include "bullet.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CPlayer::CPlayer()
{
	// クリア
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	// タイプの設定
	SetType(TYPE_PLAYER);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CPlayer::~CPlayer()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CPlayer::Init()
{
	// 移動量の初期化
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	CObject2D::Init();
	// テクスチャの設定
	CObject2D::SetTexture(CTexture::TEXTURE_PLAYER);

	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CPlayer::Update()
{
	CObject2D::Update();

	Control_();
}

//--------------------------------------------------
// 制御
//--------------------------------------------------
void CPlayer::Control_()
{
	CInputKeyboard *pInputKeyoard = CManager::GetInputKeyboard();

	if (pInputKeyoard->GetPress(DIK_A))
	{//Aキーが押されたとき
		if (pInputKeyoard->GetPress(DIK_W))
		{
			//移動量を更新(加速)
			m_move.x += sinf(-D3DX_PI * 0.75f);
			m_move.y += cosf(-D3DX_PI * 0.75f);
		}
		else if (pInputKeyoard->GetPress(DIK_S))
		{
			m_move.x += sinf(-D3DX_PI * 0.25f);
			m_move.y += cosf(D3DX_PI * 0.25f);
		}
		else
		{
			m_move.x += sinf(-D3DX_PI * 0.5f);
			m_move.y += cosf(-D3DX_PI * 0.5f);
		}
	}
	else if (pInputKeyoard->GetPress(DIK_D))
	{//Dキーが押されたとき
		if (pInputKeyoard->GetPress(DIK_W))
		{
			m_move.x += sinf(D3DX_PI * 0.75f);
			m_move.y += cosf(D3DX_PI * 0.75f);
		}
		else if (pInputKeyoard->GetPress(DIK_S))
		{
			m_move.x += sinf(D3DX_PI * 0.25f);
			m_move.y += cosf(D3DX_PI * 0.25f);
		}
		else
		{
			m_move.x += sinf(D3DX_PI * 0.5f);
			m_move.y += cosf(D3DX_PI * 0.5f);
		}
	}
	else if (pInputKeyoard->GetPress(DIK_W))
	{//Wキーが押された
		if (pInputKeyoard->GetPress(DIK_A))
		{
			m_move.x += sinf(-D3DX_PI * 0.75f);
			m_move.y += cosf(-D3DX_PI * 0.75f);
		}
		else if (pInputKeyoard->GetPress(DIK_D))
		{
			m_move.x += sinf(D3DX_PI * 0.75f);
			m_move.y += cosf(D3DX_PI * 0.75f);
		}
		else
		{
			m_move.x += sinf(D3DX_PI * 1.0f);
			m_move.y += cosf(D3DX_PI * 1.0f);
		}
	}
	else if (pInputKeyoard->GetPress(DIK_S))
	{//Sキーが押された
		if (pInputKeyoard->GetPress(DIK_A))
		{
			m_move.x += sinf(-D3DX_PI * 0.25f);
			m_move.y += cosf(-D3DX_PI * 0.25f);
		}
		else if (pInputKeyoard->GetPress(DIK_D))
		{
			m_move.x += sinf(D3DX_PI * 0.25f);
			m_move.y += cosf(D3DX_PI * 0.25f);
		}
		else
		{
			m_move.x += sinf(D3DX_PI * 0.0f);
			m_move.y += cosf(D3DX_PI * 0.0f);
		}
	}

	CObject2D::MovePos(m_move);

	// 移動量を更新(減衰)
	m_move.x += (0.0f - m_move.x) * 0.1f;
	m_move.y += (0.0f - m_move.y) * 0.1f;
	m_move.z += (0.0f - m_move.z) * 0.1f;

	if (pInputKeyoard->GetTrigger(DIK_SPACE))
	{
		CBullet::Create(D3DXVECTOR3(0.0f, 0.0f, 20.0f), D3DXVECTOR3(10.0f, 10.0f, 0.0f))->SetPos(CObject2D::GetPos());
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CPlayer *CPlayer::Create(const D3DXVECTOR3 size)
{
	CPlayer *pPlayer;
	pPlayer = new CPlayer;

	if (pPlayer != nullptr)
	{
		pPlayer->Init();
		pPlayer->SetSize(size);
	}
	else
	{
		assert(false);
	}

	return pPlayer;
}
