//==================================================
// back_model.h
// Author: Buriya Kota
//==================================================
#ifndef _BACK_MODEL_H_
#define _BACK_MODEL_H_

//**************************************************
// インクルード
//**************************************************
#include "object3D.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CBackModel : public CObject3D
{
public:
	enum MODEL_TYPE
	{
		MODEL_TYPE_BILL = 0,			// ビル
		MODEL_TYPE_INTERSECTION,		// 交差点
		MODEL_TYPE_HIGHWAY,				// 高速道路
		MODEL_TYPE_MAX
	};

	explicit CBackModel(int nPriority = PRIORITY_BG);
	~CBackModel() override;

	void Update() override;

	// モデルの読み込み
	static void Load(const char *pFdata);

	static CBackModel *Create(MODEL_TYPE type);

};

#endif	// _BACK_MODEL_H_