//==================================================
// tutorial.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "input_keyboard.h"
#include "input_joypad.h"

#include "score.h"
#include "tutorial.h"
#include "fade.h"
#include "ranking.h"

#include "life_manager.h"
#include "player3D.h"
#include "enemy3D.h"
#include "tutorial_enemy.h"
#include "back_model.h"
#include "polygon3D.h"
#include "lockonUI_manager.h"

// jsonのinclude
#include "nlohmann/json.hpp"
#include <fstream>

namespace nl = nlohmann;

static nl::json EnemyList;		//　リストの生成
static nl::json PlayerList;		//　リストの生成

//**************************************************
// 静的メンバ変数
//**************************************************
CScore *CTutorial::m_pScore = nullptr;
CPlayer3D *CTutorial::m_pPlayer3D = nullptr;
CLockOnUIManager *CTutorial::m_pLockOnUIManager = nullptr;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CTutorial::CTutorial()
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CTutorial::~CTutorial()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CTutorial::Init()
{
	m_time = 0;


	// エネミー
	for (int nCnt = 0; nCnt < 5; nCnt++)
	{
		m_pEnemy[nCnt] = CTutorialEnemy::Create();
		m_pEnemy[nCnt]->SetPos(D3DXVECTOR3(900.0f + 300.0f * nCnt, 300, 300.0f));
		m_pEnemy[nCnt]->SetMove(D3DXVECTOR3(-4.0f, 0.0f, 0.0f));
		m_pEnemy[nCnt]->SetModel("data/MODEL/cobra.x");
	}

	// スコア
	m_pScore = CScore::Create(D3DXVECTOR3(60.0f, 50.0f, 0.0f), D3DXVECTOR3(30.0f, 60.0f, 0.0f));
	m_pScore->SetScore(0);

	// ロックオンの残弾数
	m_pLockOnUIManager = CLockOnUIManager::Create();

	// 体力
	CLifeManager::Create(3);

	// 背景
	CBackModel::Load("data/FILE/background.json");

	// プレイヤー
	LoadPlayer("data/FILE/player.json");

	// 操作説明
	CObject2D *pObject2D = CObject2D::Create();
	pObject2D->SetPos(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, CManager::SCREEN_HEIGHT * 0.85f, 0.0f));
	pObject2D->SetSize(D3DXVECTOR3((float)CManager::SCREEN_WIDTH, CManager::SCREEN_HEIGHT * 0.25f, 0.0f));
	pObject2D->SetTexture(CTexture::TEXTURE_TUTORIAL_CONTROLER);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CTutorial::Uninit()
{
	// リリースはリリースオールでやってある
	m_pScore = nullptr;
	m_pPlayer3D = nullptr;
	m_pLockOnUIManager = nullptr;

	CObject::Release();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CTutorial::Update()
{
	m_time++;

	if(m_time == 600)
	{
		for (int nCnt = 0; nCnt < 5; nCnt++)
		{
			m_pEnemy[nCnt] = CTutorialEnemy::Create();
			m_pEnemy[nCnt]->SetPos(D3DXVECTOR3(900.0f + 300.0f * nCnt, 300, 300.0f));
			m_pEnemy[nCnt]->SetMove(D3DXVECTOR3(-4.0f, 0.0f, 0.0f));
			m_pEnemy[nCnt]->SetModel("data/MODEL/cobra.x");
		}

		m_time = 0;
	}

	CInputKeyboard *pInputKeyoard = CManager::GetInputKeyboard();
	CInputJoyPad *pInputJoyPad = CManager::GetInputJoyPad();

	if (pInputKeyoard->GetTrigger(DIK_BACKSPACE) || pInputJoyPad->GetJoypadTrigger(pInputJoyPad->JOYKEY_B, 0))
	{
		CFade::GetInstance()->SetFade(CManager::MODE_TITLE);
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CTutorial* CTutorial::Create()
{
	CTutorial *pTutorial;
	pTutorial = new CTutorial;

	if (pTutorial != nullptr)
	{
		pTutorial->Init();
	}
	else
	{
		assert(false);
	}

	return pTutorial;
}

//--------------------------------------------------
// 読み込み
//--------------------------------------------------
void CTutorial::LoadPlayer(const char *pFdata)
{
	// ファイルオープン
	std::ifstream ifs(pFdata);

	if (ifs)
	{// 開けたら
		ifs >> PlayerList;

		// 位置
		D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
		// 大きさ
		D3DXVECTOR3 move(0.0f, 0.0f, 0.0f);
		// 角度
		D3DXVECTOR3 rot(0.0f, 0.0f, 0.0f);
		// 体力
		int Life = 0;

		std::string name = "PLAYER";

		pos = D3DXVECTOR3(PlayerList[name]["POS"]["X"], PlayerList[name]["POS"]["Y"], PlayerList[name]["POS"]["Z"]);
		move = D3DXVECTOR3(PlayerList[name]["MOVE"]["X"], PlayerList[name]["MOVE"]["Y"], PlayerList[name]["MOVE"]["Z"]);
		rot = D3DXVECTOR3(PlayerList[name]["ROT"]["X"], PlayerList[name]["ROT"]["Y"], PlayerList[name]["ROT"]["Z"]);
		Life = PlayerList[name]["LIFE"];

		m_pPlayer3D = CPlayer3D::Create();
		m_pPlayer3D->SetPos(pos);
		m_pPlayer3D->SetMove(move);
		m_pPlayer3D->SetRot(rot);
		m_pPlayer3D->SetLife(Life);
		m_pPlayer3D->SetModel("data/MODEL/X_wing.x");
	}
}
