//**************************************************
// 
// texture.cpp
// Author  : katsuki mizuki
// 
//**************************************************

//==================================================
// インクルード
//==================================================
#include "manager.h"
#include "renderer.h"
#include "texture.h"

#include <assert.h>

//==================================================
// 定義
//==================================================
const char* CTexture::s_FileName[] =
{// テクスチャのパス
	"data/TEXTURE/hasegawa_nagi.png",		// プレイヤー
	"data/TEXTURE/doragon.png",				// エネミー
	"data/TEXTURE/bullet.png",				// 弾
	"data/TEXTURE/pipo-btleffect036.png",	// 爆発
	"data/TEXTURE/road.png",				// 道
	"data/TEXTURE/lock_on_cursor.png",		// ロックオンカーソル
	"data/TEXTURE/lockOn.png",				// ロックオン
	"data/TEXTURE/Number_002.png",			// 数
	"data/TEXTURE/TITLE.png",				// タイトル
	"data/TEXTURE/BG_TITLE.png",			// BGタイトル
	"data/TEXTURE/START.png",				// スタート
	"data/TEXTURE/TUTORIAL.png",			// チュートリアル
	"data/TEXTURE/GAMEOVER.png",			// リザルト
	"data/TEXTURE/life.png",				// ライフ
	"data/TEXTURE/Rank_1to5_004.png",		// 順位
	"data/TEXTURE/ranking.png",				// ランキング背景
	"data/TEXTURE/tutorial_control.png",	// チュートリアルの操作方法
	"data/TEXTURE/enemy_bullet.png",		// エネミーの弾
	"data/TEXTURE/restart.png",				// 初めから
	"data/TEXTURE/backTitle.png",			// タイトルへ
	"data/TEXTURE/close.png",				// 閉じる
	"data/TEXTURE/pause.png",				// ポーズ
};

static_assert(sizeof(CTexture::s_FileName) / sizeof(CTexture::s_FileName[0]) == CTexture::TEXTURE_MAX, "aho");

//--------------------------------------------------
// デフォルトコンストラクタ
//--------------------------------------------------
CTexture::CTexture() :
	s_pTexture()
{
	memset(s_pTexture, 0, sizeof(s_pTexture));
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CTexture::~CTexture()
{
}

//--------------------------------------------------
// 全ての読み込み
//--------------------------------------------------
void CTexture::LoadAll()
{
	// デバイスへのポインタの取得
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

	for (int i = 0; i < TEXTURE_MAX; ++i)
	{
		if (s_pTexture[i] != nullptr)
		{// テクスチャの読み込みがされている
			continue;
		}

		// テクスチャの読み込み
		D3DXCreateTextureFromFile(pDevice,
			s_FileName[i],
			&s_pTexture[i]);
	}
}

//--------------------------------------------------
// 読み込み
//--------------------------------------------------
void CTexture::Load(TEXTURE inTexture)
{
	assert(inTexture >= 0 && inTexture < TEXTURE_MAX);

	if (s_pTexture[inTexture] != nullptr)
	{// テクスチャの読み込みがされている
		return;
	}

	// デバイスへのポインタの取得
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

	// テクスチャの読み込み
	D3DXCreateTextureFromFile(pDevice,
		s_FileName[inTexture],
		&s_pTexture[inTexture]);
}

//--------------------------------------------------
// 全ての解放
//--------------------------------------------------
void CTexture::ReleaseAll(void)
{
	for (int i = 0; i < TEXTURE_MAX; ++i)
	{
		if (s_pTexture[i] != nullptr)
		{// テクスチャの解放
			s_pTexture[i]->Release();
			s_pTexture[i] = nullptr;
		}
	}
}

//--------------------------------------------------
// 解放
//--------------------------------------------------
void CTexture::Release(TEXTURE inTexture)
{
	assert(inTexture >= 0 && inTexture < TEXTURE_MAX);

	if (s_pTexture[inTexture] != nullptr)
	{// テクスチャの解放
		s_pTexture[inTexture]->Release();
		s_pTexture[inTexture] = nullptr;
	}
}

//--------------------------------------------------
// 取得
//--------------------------------------------------
LPDIRECT3DTEXTURE9 CTexture::GetTexture(TEXTURE inTexture)
{
	if (inTexture == TEXTURE_NONE)
	{// テクスチャを使用しない
		return nullptr;
	}

	assert(inTexture >= 0 && inTexture < TEXTURE_MAX);

	// 読み込み
	Load(inTexture);

	return s_pTexture[inTexture];
}
