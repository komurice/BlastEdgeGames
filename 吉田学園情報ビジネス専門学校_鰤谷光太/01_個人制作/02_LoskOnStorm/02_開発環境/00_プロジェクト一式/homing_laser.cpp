//==================================================
// homing_laser.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "game.h"
#include "tutorial.h"
#include "score.h"
#include "input_keyboard.h"
#include "utility.h"
#include "object.h"
#include "homing_laser.h"
#include "explosion.h"
#include "enemy3D.h"
#include "effect.h"
#include "mesh_trajectory.h"
#include "add_lockon_item.h"
#include "sound.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CHomingLaser::CHomingLaser()
{
	// タイプ設定
	SetType(TYPE_HOMING_LASER);
	// ポインタの初期化
	m_pEnemy = nullptr;
	// ポインタの初期化
	m_pMeshTrajectory = nullptr;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CHomingLaser::~CHomingLaser()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CHomingLaser::Init()
{
	m_nTime = 0;

	CBullet::Init();
	// テクスチャの設定
	CPolygon3D::SetTexture(CTexture::TEXTURE_BULLET);

	// 動的確保
	m_pMeshTrajectory = new CMeshTrajectory;

	if (m_pMeshTrajectory != nullptr)
	{// 終了処理
		m_pMeshTrajectory->Init();
	}

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CHomingLaser::Uninit()
{
	CBullet::Uninit();

	if (m_pMeshTrajectory != nullptr)
	{// 終了処理
		m_pMeshTrajectory->Uninit();
		m_pMeshTrajectory = nullptr;
	}
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CHomingLaser::Update()
{
	CPolygon3D::Update();

	if (m_pMeshTrajectory != nullptr)
	{// 更新処理
		m_pMeshTrajectory->Update();
	}

	// 撃つ方向
	D3DXVECTOR3 move = GetMove();

	if (m_pEnemy == nullptr || m_pEnemy->IsDeleted())
	{// 弾が飛んでる途中で狙う敵がいなくなったら今向いてる方向へそのまま弾をとばす
		MovePos(move);
		// 軌跡
		m_pMeshTrajectory->AddVtx(GetPos(), move);

		move *= 1.1f;
		SetMove(move);
		if (GetPos().z >= 1000.0f || GetPos().z <= -1000.0f
			|| GetPos().x >= 1000.0f || GetPos().x <= -1000.0f)
		{
			Uninit();
		}

		return;
	}

	// posの取得
	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 size = GetSize();
	D3DXVECTOR3 targetPos = m_pEnemy->GetPos();
	D3DXVECTOR3 targetSize = m_pEnemy->GetSize();

	// 方向の計算
	D3DXVECTOR3 vec = targetPos - pos;
	D3DXVECTOR3 nom;
	D3DXVec3Normalize(&nom, &vec);

	m_nTime++;
	float homingPower = 10.0f;

	if (m_nTime < 30)
	{
		move = nom * homingPower + m_FirstMove * (1.0f - m_nTime / 30.0f);
	}
	else
	{
		float homingAccel = (1.0f + (m_nTime - 30) * 0.5f);

		if (D3DXVec3Length(&vec) < homingPower * homingAccel)
		{
			move = vec;
		}
		else
		{
			move = nom * homingPower * homingAccel;
		}
	}

	SetMove(move);

	bool isHit = IsCollisionSphere(targetPos, pos, targetSize, size);
	if (isHit)
	{// 当たった時
		CExplosion::Create(D3DXVECTOR3(60.0f, 60.0f, 0.0f))->SetPos(m_pEnemy->GetPos());

		CObject **pObject = CObject::GetCObject(PRIORITY_BULLET);

		// 止まっちゃう弾対策
		for (int nCnt = 0; nCnt < MAX_OBjECT; nCnt++)
		{// すべてのオブジェクトを
			if (pObject[nCnt] == nullptr || pObject[nCnt]->IsDeleted() || pObject[nCnt] == this)
			{
				continue;
			}

			CObject::TYPE_3D objType = pObject[nCnt]->GetType();
			if (objType != CObject::TYPE_HOMING_LASER)
			{// 弾を探す
				continue;
			}

			// ダイナミックキャスト
			CHomingLaser* pHomingLaser = dynamic_cast<CHomingLaser*>(pObject[nCnt]);
			if ((pHomingLaser == nullptr))
			{
				assert(false);
				continue;
			}

			// 狙ってる敵がいなくなった弾があったら目標の敵をなくす
			if (pHomingLaser->m_pEnemy == m_pEnemy)
			{
				pHomingLaser->m_pEnemy = nullptr;
			}
		}

		if (CGame::GetScore() != nullptr)
		{
			CGame::GetScore()->AddScore(500);
		}

		if (CTutorial::GetScore() != nullptr)
		{
			CTutorial::GetScore()->AddScore(500);
		}

		// サウンド
		CManager::GetSound()->Play(CSound::LABEL_SE_HIT);

		if (m_pEnemy->GetEnemyType() == CEnemy3D::ENEMY_TYPE_TRIDORON)
		{
			CAddLockOnItem *pAddLockOnItem;
			pAddLockOnItem = CAddLockOnItem::Create(D3DXVECTOR3(0.0f, 0.0f, 3.0f));
			pAddLockOnItem->SetPos(m_pEnemy->GetPos());
			pAddLockOnItem->SetModel("data/MODEL/add_llockon_item.x");
		}

		m_pEnemy->SetHomingLaser(nullptr);

		// エネミニライフを減らす
		m_pEnemy->SubLife(1);
		if (m_pEnemy->GetLife() <= 0)
		{// 0以下になったら削除
			m_pEnemy->Uninit();
		}

		Uninit();

		return;
	}

	MovePos(move);

	// 軌跡
	m_pMeshTrajectory->AddVtx(GetPos(), move);
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CHomingLaser::Draw()
{
	CBullet::Draw();

	if (m_pMeshTrajectory != nullptr)
	{// 終了処理
		m_pMeshTrajectory->Draw();
	}
}

//--------------------------------------------------
// エネミーが死んだときにロックオン数を元に戻す
//--------------------------------------------------
void CHomingLaser::DeadEnemy(CEnemy3D* enemy)
{
	if ((m_pEnemy != nullptr) && (m_pEnemy == enemy))
	{
		m_pEnemy = nullptr;
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CHomingLaser *CHomingLaser::Create(const D3DXVECTOR3 size, const D3DXVECTOR3& move, CEnemy3D* pEnemy)
{
	CHomingLaser *pHomingLaser;
	pHomingLaser = new CHomingLaser;

	if (pHomingLaser != nullptr)
	{
		pHomingLaser->Init();
		pHomingLaser->SetSize(size);
		pHomingLaser->SetBillboard(true);
		pHomingLaser->SetMove(move);
		pHomingLaser->m_FirstMove = move;
		pHomingLaser->m_pEnemy = pEnemy;
		pEnemy->SetHomingLaser(pHomingLaser);
	}
	else
	{
		assert(false);
	}

	return pHomingLaser;
}
