//==================================================
// hornet.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include "manager.h"
#include "hornet.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CHornet::CHornet()
{
	SetEnemyType(ENEMY_TYPE_HORNET);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CHornet::~CHornet()
{
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CHornet::Update()
{
	CEnemy3D::Update();

	MoveHornet_();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CHornet *CHornet::Create()
{
	CHornet *pHornet;
	pHornet = new CHornet;

	if (pHornet != nullptr)
	{
		pHornet->Init();
	}
	else
	{
		assert(false);
	}

	return pHornet;
}

//--------------------------------------------------
// ホーネットの動き
//--------------------------------------------------
void CHornet::MoveHornet_()
{
	D3DXVECTOR3 move = GetMove();
	SetMove(move);

	m_nCntFrame++;

	// 
	if (m_nCntFrame < 300)
	{
		move.z += 0.3f;
		SetMove(move);
	}
	else
	{
		SetMove(move);
	}

	MovePos(move);

	D3DXVECTOR3 pos = GetPos();

	if (pos.z > 1000.0f)
	{
		Uninit();
	}
}
