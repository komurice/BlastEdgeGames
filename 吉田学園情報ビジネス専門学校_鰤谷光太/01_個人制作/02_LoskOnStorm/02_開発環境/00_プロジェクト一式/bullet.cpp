//==================================================
// bullet.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "game.h"
#include "tutorial.h"
#include "score.h"
#include "input_keyboard.h"
#include "utility.h"
#include "object.h"
#include "bullet.h"
#include "enemy3D.h"
#include "explosion.h"
#include "fade.h"
#include "add_lockon_item.h"
#include "sound.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CBullet::CBullet(int nPriority /* =2 */) : CPolygon3D(nPriority)
{
	// タイプ設定
	SetType(TYPE_BULLET);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CBullet::~CBullet()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CBullet::Init()
{
	m_time = 0;

	CPolygon3D::Init();
	// テクスチャの設定
	CPolygon3D::SetTexture(CTexture::TEXTURE_BULLET);

	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CBullet::Update()
{
	m_time++;

	CPolygon3D::Update();

	CollisionEnemy_();
	CollisionBoss_();

	BulletMove_();
}

//--------------------------------------------------
// 制御
//--------------------------------------------------
void CBullet::BulletMove_()
{
	D3DXVECTOR3 move = GetMove();
	SetMove(move);

	MovePos(move);

	if (GetPos().z >= 1000.0f)
	{
		Uninit();
	}
}

//--------------------------------------------------
// エネミーとの判定
//--------------------------------------------------
void CBullet::CollisionEnemy_()
{
	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 size = GetSize();

	CObject **pObject = CObject::GetCObject(PRIORITY_OBJECT);

	for (int nCnt = 0; nCnt < CObject::MAX_OBjECT; nCnt++)
	{
		if (pObject[nCnt] == nullptr || pObject[nCnt]->IsDeleted())
		{
			continue;
		}

		CObject::TYPE_3D objType = pObject[nCnt]->GetType();
		if (objType != CObject::TYPE_ENEMY)
		{// 球の当たり判定
			continue;
		}

		// ダイナミックキャスト
		CEnemy3D* pEnemy = dynamic_cast<CEnemy3D*>(pObject[nCnt]);
		if ((pEnemy == nullptr))
		{
			assert(false);
			continue;
		}

		D3DXVECTOR3 targetPos = pEnemy->GetPos();
		D3DXVECTOR3 targetSize = pEnemy->GetSize();

		bool isHit = IsCollisionSphere(targetPos, pos, targetSize, size);
		if (isHit)
		{// 当たった時
			// サウンド
			CManager::GetSound()->Play(CSound::LABEL_SE_HIT);

			CExplosion::Create(D3DXVECTOR3(60.0f, 60.0f, 0.0f))->SetPos(pEnemy->GetPos());

			if (pEnemy->GetEnemyType() == CEnemy3D::ENEMY_TYPE_TRIDORON)
			{
				CAddLockOnItem *pAddLockOnItem;
				pAddLockOnItem = CAddLockOnItem::Create(D3DXVECTOR3(0.0f, 0.0f, 3.0f));
				pAddLockOnItem->SetPos(pEnemy->GetPos());
				pAddLockOnItem->SetModel("data/MODEL/add_llockon_item.x");
			}

			pEnemy->SubLife(1);
			if (pEnemy->GetLife() <= 0)
			{
				if (CGame::GetScore() != nullptr)
				{
					CGame::GetScore()->AddScore(100);
				}

				if (CTutorial::GetScore() != nullptr)
				{
					CTutorial::GetScore()->AddScore(100);
				}

				pEnemy->Uninit();
			}

			Uninit();
		}
	}
}

//--------------------------------------------------
// ボスとの判定
//--------------------------------------------------
void CBullet::CollisionBoss_()
{
	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 size = GetSize();

	CObject **pObject = CObject::GetCObject(PRIORITY_OBJECT);

	for (int nCnt = 0; nCnt < CObject::MAX_OBjECT; nCnt++)
	{
		if (pObject[nCnt] == nullptr || pObject[nCnt]->IsDeleted())
		{
			continue;
		}

		CObject::TYPE_3D objType = pObject[nCnt]->GetType();
		if (objType != CObject::TYPE_BOSS)
		{// 球の当たり判定
			continue;
		}

		// ダイナミックキャスト
		CEnemy3D* pEnemy = dynamic_cast<CEnemy3D*>(pObject[nCnt]);
		if ((pEnemy == nullptr))
		{
			assert(false);
			continue;
		}

		D3DXVECTOR3 targetPos = pEnemy->GetPos();
		D3DXVECTOR3 targetSize = pEnemy->GetSize();
		bool isHit = IsCollisionSphere(targetPos, pos, targetSize, size);

		if (isHit)
		{// 当たった時
			// サウンド
			CManager::GetSound()->Play(CSound::LABEL_SE_HIT);

			CExplosion::Create(D3DXVECTOR3(60.0f, 60.0f, 0.0f))->SetPos(pEnemy->GetPos());

			pEnemy->SubLife(1);
			if (pEnemy->GetLife() <= 0)
			{
				CGame::GetScore()->AddScore(10000);
				pEnemy->Uninit();
				CFade::GetInstance()->SetFade(CManager::MODE_RANKING);
			}

			Uninit();
		}
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CBullet *CBullet::Create(const D3DXVECTOR3 move, const D3DXVECTOR3 size)
{
	CBullet *pBullet;
 	pBullet = new CBullet;

	if (pBullet != nullptr)
	{
		pBullet->Init();
		pBullet->SetMove(move);
		pBullet->SetSize(size);
		pBullet->SetBillboard(true);
	}
	else
	{
		assert(false);
	}

	return pBullet;
}
