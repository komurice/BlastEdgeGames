//==================================================
// object.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "object.h"
#include "object2D.h"
#include "renderer.h"

//**************************************************
// 静的メンバ変数
//**************************************************
int CObject::m_nNumAll = 0;
CObject *CObject::ms_pObject[MAX_PRIO][MAX_OBjECT] = {};

//--------------------------------------------------
// プライオリティを使ったコンストラクタ
//--------------------------------------------------
CObject::CObject(int nPriority /* PRIORITY_3 */)
{
	// 削除されたかどうか
	m_bDeleted = false;

	for (int nCnt = 0; nCnt < MAX_OBjECT; nCnt++)
	{
		if (ms_pObject[nPriority][nCnt] == nullptr)
		{// 自身のポインタを代入
			ms_pObject[nPriority][nCnt] = this;
			m_Index = nCnt;
			m_Priority = nPriority;
			// エネミーの総数
			m_nNumAll++;
			break;
		}
	}
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CObject::~CObject()
{
}

//--------------------------------------------------
// すべての破棄
//--------------------------------------------------
void CObject::ReleaseAll()
{
	for (int nCntPrio = 0; nCntPrio < MAX_PRIO; nCntPrio++)
	{
		for (int nCnt = 0; nCnt < MAX_OBjECT; nCnt++)
		{
			if (ms_pObject[nCntPrio][nCnt] == nullptr)
			{
				continue;
			}

			// 終了
			ms_pObject[nCntPrio][nCnt]->Uninit();
		}
	}
	//deleted == true をdelete & null代入
	for (int nCntPrio = 0; nCntPrio < MAX_PRIO; nCntPrio++)
	{
		for (int nCnt = 0; nCnt < MAX_OBjECT; nCnt++)
		{
			if (ms_pObject[nCntPrio][nCnt] != nullptr && ms_pObject[nCntPrio][nCnt]->IsDeleted() == true)
			{
				delete ms_pObject[nCntPrio][nCnt];
				ms_pObject[nCntPrio][nCnt] = nullptr;
			}
		}
	}
}

//--------------------------------------------------
// すべての更新
//--------------------------------------------------
void CObject::UpdateAll()
{
	for (int nCntPrio = 0; nCntPrio < MAX_PRIO; nCntPrio++)
	{
		for (int nCnt = 0; nCnt < MAX_OBjECT; nCnt++)
		{//		deleted == trueで無視
			if (ms_pObject[nCntPrio][nCnt] == nullptr || ms_pObject[nCntPrio][nCnt]->IsDeleted())
			{
				continue;
			}
			// 更新
			ms_pObject[nCntPrio][nCnt]->Update();
		}
	}
	//deleted == true を delete & null代入
	for (int nCntPrio = 0; nCntPrio < MAX_PRIO; nCntPrio++)
	{
		for (int nCnt = 0; nCnt < MAX_OBjECT; nCnt++)
		{
			if (ms_pObject[nCntPrio][nCnt] != nullptr && ms_pObject[nCntPrio][nCnt]->IsDeleted())
			{
				delete ms_pObject[nCntPrio][nCnt];
				ms_pObject[nCntPrio][nCnt] = nullptr;
			}
		}
	}
}

//--------------------------------------------------
// すべての描画
//--------------------------------------------------
void CObject::DrawAll()
{
	for (int nCntPrio = 0; nCntPrio < MAX_PRIO; nCntPrio++)
	{
		for (int nCnt = 0; nCnt < MAX_OBjECT; nCnt++)
		{//		deleted == trueで無視
			if (ms_pObject[nCntPrio][nCnt] == nullptr || ms_pObject[nCntPrio][nCnt]->IsDeleted())
			{
				continue;
			}
			// 描画
			ms_pObject[nCntPrio][nCnt]->Draw();
		}
	}
}

//--------------------------------------------------
// モード以外をリリース
//--------------------------------------------------
void CObject::ReleaseWithoutMode()
{
	for (int nCntPrio = 0; nCntPrio < MAX_PRIO; nCntPrio++)
	{
		for (int nCnt = 0; nCnt < MAX_OBjECT; nCnt++)
		{
			if (ms_pObject[nCntPrio][nCnt] == nullptr)
			{
				continue;
			}

			if (ms_pObject[nCntPrio][nCnt]->GetType() == TYPE_MODE)
			{
				continue;
			}

			// 終了
			ms_pObject[nCntPrio][nCnt]->Uninit();
		}
	}
	//deleted == true をdelete & null代入
	for (int nCntPrio = 0; nCntPrio < MAX_PRIO; nCntPrio++)
	{
		for (int nCnt = 0; nCnt < MAX_OBjECT; nCnt++)
		{
			if (ms_pObject[nCntPrio][nCnt] != nullptr && ms_pObject[nCntPrio][nCnt]->IsDeleted() == true)
			{
				delete ms_pObject[nCntPrio][nCnt];
				ms_pObject[nCntPrio][nCnt] = nullptr;
			}
		}
	}
}

//--------------------------------------------------
// すべての描画
//--------------------------------------------------
void CObject::Release()
{
	if (ms_pObject[m_Priority][m_Index] != nullptr)
	{
		m_bDeleted = true;
		m_nNumAll--;
	}
}

//--------------------------------------------------
// 球の当たり判定
//--------------------------------------------------
bool IsCollisionSphere(D3DXVECTOR3 targetPos, D3DXVECTOR3 pos, D3DXVECTOR3 targetSize, D3DXVECTOR3 size)
{
	bool bIsLanding = false;

	// 目的の距離
	float fTargetDistance = sqrtf((targetPos.x - pos.x) * (targetPos.x - pos.x) +
		(targetPos.y - pos.y) * (targetPos.y - pos.y) +
		(targetPos.z - pos.z) * (targetPos.z - pos.z));
	// 現在の距離
	float fCurrentDistance = sqrtf((targetSize.x + size.x) * (targetSize.x + size.x) +
		(targetSize.y + size.y) * (targetSize.y + size.y) +
		(targetSize.z + size.z) * (targetSize.z + size.z));

	if (fTargetDistance < fCurrentDistance)
	{
		bIsLanding = true;
	}

	return bIsLanding;
}

//--------------------------------------------------
// 矩形の当たり判定
//--------------------------------------------------
bool IsCollision(D3DXVECTOR3 targetPos, D3DXVECTOR3 pos, D3DXVECTOR3 targetSize, D3DXVECTOR3 size)
{
	bool bIsLanding = false;

	if (pos.x + size.x / 2.0f >= targetPos.x - targetSize.x / 2.0f
		&& pos.x - size.x / 2.0f <= targetPos.x + targetSize.x / 2.0f
		&& pos.y - size.y / 2.0f <= targetPos.y + targetSize.y / 2.0f
		&& pos.y + size.y / 2.0f >= targetPos.y - targetSize.y / 2.0f)
	{
		bIsLanding = true;
	}

	return bIsLanding;
}
