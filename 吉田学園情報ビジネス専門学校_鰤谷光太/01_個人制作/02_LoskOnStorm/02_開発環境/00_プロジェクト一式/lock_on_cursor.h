//==================================================
// lock_on_cursor.h
// Author: Buriya Kota
//==================================================
#ifndef _LOCK_ON_CURSOR_H_
#define _LOCK_ON_CURSOR_H_

//**************************************************
// インクルード
//**************************************************
#include "polygon3D.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CEnemy3D;
class CLockOnUIManager;

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// マクロ定義
//**************************************************
#define MAX_TARGET		(8)
#define INIT_TARGET		(3)

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CLockOnCursor : public CPolygon3D
{
public:
	explicit CLockOnCursor(int nPriority = PRIORITY_EFFECT);
	~CLockOnCursor() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	// セッター
	void SetWorldMtx(D3DXMATRIX playerWorldMtx) { m_pPlayerWorldMtx = playerWorldMtx; }

	// ゲッター
	CEnemy3D **GetLockOnTarget() { return m_pEnemy; }
	int GetNumLockOn() { return m_nCurrentNum; }

	int GetNowMaxLockOn() { return m_nLimitNum; }

	// ロックオン数を増やす
	void AddLockOn();

	static CLockOnCursor *Create(const D3DXVECTOR3 size, int nPriority);

	// ロックオンしてるエネミーが死んだとき
	void DeadEnemy(CEnemy3D* enemy);

private:
	// テクスチャへのポインタ
	CTexture::TEXTURE m_pTexture;
	// プレイヤーのマトリックス
	D3DXMATRIX m_pPlayerWorldMtx;
	// エネミーのポインタ
	CEnemy3D* m_pEnemy[MAX_TARGET];
	// 現在のロックオン限界数
	int m_nLimitNum;
	// 現在のロックオン数
	int m_nCurrentNum;
	// ロックしているかどうか
	bool m_bLocked;

};

#endif	// _LOCK_ON_CURSOR_H_