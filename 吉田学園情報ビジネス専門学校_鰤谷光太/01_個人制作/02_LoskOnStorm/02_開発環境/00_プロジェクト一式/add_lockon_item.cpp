//==================================================
// add_lockon_item.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "add_lockon_item.h"
#include "player3D.h"
#include "lock_on_cursor.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CAddLockOnItem::CAddLockOnItem()
{
	// タイプの設定
	SetType(TYPE_ITEM);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CAddLockOnItem::~CAddLockOnItem()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CAddLockOnItem::Init()
{
	CObject3D::Init();

	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CAddLockOnItem::Update()
{
	CObject3D::Update();

	D3DXVECTOR3 move = GetMove();

	move.z += -0.1f;
	SetMove(move);

	MovePos(move);
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CAddLockOnItem *CAddLockOnItem::Create(D3DXVECTOR3 move)
{
	CAddLockOnItem *pAddLockOnItem = nullptr;
	pAddLockOnItem = new CAddLockOnItem;

	if (pAddLockOnItem != nullptr)
	{
		pAddLockOnItem->Init();
		pAddLockOnItem->SetMove(move);
	}
	else
	{
		assert(false);
	}

	return pAddLockOnItem;
}
