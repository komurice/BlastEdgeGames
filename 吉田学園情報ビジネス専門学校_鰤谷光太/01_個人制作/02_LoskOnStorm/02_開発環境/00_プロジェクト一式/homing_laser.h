//==================================================
// homing_laser.h
// Author: Buriya Kota
//==================================================
#ifndef _HOMING_LASER_H_
#define _HOMING_LASER_H_

//**************************************************
// インクルード
//**************************************************
#include "bullet.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CEnemy3D;
class CMeshTrajectory;

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CHomingLaser : public CBullet
{
public:
	CHomingLaser();
	~CHomingLaser() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	// レーザーを飛ばした相手がいなくなったとき
	void DeadEnemy(CEnemy3D* enemy);

	static CHomingLaser *Create(const D3DXVECTOR3 size, const D3DXVECTOR3& move, CEnemy3D* pEnemy);

private:
	// テクスチャへのポインタ
	CTexture::TEXTURE m_pTexture;
	// 目的のpos
	D3DXVECTOR3 m_targetPos;
	// エネミーのポインタ
	CEnemy3D* m_pEnemy;
	// 軌跡のポインタ
	CMeshTrajectory* m_pMeshTrajectory;
	// タイマー
	int m_nTime;
	// 初期の動き
	D3DXVECTOR3 m_FirstMove;
};

#endif	// _HOMING_LASER_H_