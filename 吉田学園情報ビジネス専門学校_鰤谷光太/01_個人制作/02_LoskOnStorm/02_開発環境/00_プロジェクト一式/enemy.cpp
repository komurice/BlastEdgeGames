//==================================================
// enemy.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "enemy.h"
#include "manager.h"
#include "input_keyboard.h"
#include "bullet.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CEnemy::CEnemy()
{
	// クリア
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	// タイプの設定
	SetType(TYPE_ENEMY);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CEnemy::~CEnemy()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CEnemy::Init()
{
	// アニメーションカウンター
	m_nCounterAnim = 0;
	// アニメーションパターンNo.
	m_nPatternAnim = 0;

	// 移動量の初期化
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	CObject2D::Init();
	// テクスチャの設定
	CObject2D::SetTexture(CTexture::TEXTURE_ENEMY);
	// アニメーションの初期化
	CObject2D::AnimTexture(m_nPatternAnim, 0);

	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CEnemy::Update()
{
	CObject2D::Update();

	m_nCounterAnim++;

	if ((m_nCounterAnim % ANIM_SPEED) == 0)
	{
		m_nPatternAnim = (m_nPatternAnim + 1) % ENEMY_ANIM;

		CObject2D::AnimTexture(m_nPatternAnim, ENEMY_ANIM);
		m_nPatternAnim++;
	}
}

//--------------------------------------------------
// 制御
//--------------------------------------------------
void CEnemy::Control_()
{

}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CEnemy *CEnemy::Create(const D3DXVECTOR3 size)
{
	CEnemy *pEnemy;
	pEnemy = new CEnemy;

	if (pEnemy != nullptr)
	{
		pEnemy->Init();
		pEnemy->SetSize(size);
	}
	else
	{
		assert(false);
	}

	return pEnemy;
}
