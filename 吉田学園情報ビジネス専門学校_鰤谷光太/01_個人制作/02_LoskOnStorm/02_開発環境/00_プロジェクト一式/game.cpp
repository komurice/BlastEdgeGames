//==================================================
// game.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "input_keyboard.h"

#include "score.h"
#include "game.h"
#include "fade.h"
#include "ranking.h"

#include "life.h"
#include "life_manager.h"
#include "player3D.h"
#include "enemy3D.h"
#include "cobra.h"
#include "tridoron.h"
#include "snake.h"
#include "back_model.h"
#include "polygon3D.h"
#include "meshfilde.h"
#include "mesh_trajectory.h"
#include "lockonUI_manager.h"
#include "sound.h"
#include "pause.h"

// jsonのinclude
#include "nlohmann/json.hpp"
#include <fstream>

namespace nl = nlohmann;

static nl::json EnemyList;		//　リストの生成
static nl::json PlayerList;		//　リストの生成

//**************************************************
// 静的メンバ変数
//**************************************************
CScore *CGame::m_pScore = nullptr;
CLockOnUIManager *CGame::m_pLockOnUIManager = nullptr;
CPlayer3D *CGame::m_pPlayer3D = nullptr;
CPause *CGame::m_pPause = nullptr;

//**************************************************
// マクロ定義
//**************************************************
#define GROUND_POS_Y			(400)

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CGame::CGame()
{
	m_time = 0;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CGame::~CGame()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CGame::Init()
{
	CManager::GetSound()->Play(CSound::LABEL_BGM_GAME);

	m_time = 0;

	m_pScore = CScore::Create(D3DXVECTOR3(60.0f, 50.0f, 0.0f), D3DXVECTOR3(30.0f, 60.0f, 0.0f));
	m_pScore->SetScore(0);

	m_pPause = CPause::Create();

	m_pLockOnUIManager = CLockOnUIManager::Create();

	CLifeManager::Create(3);

	CBackModel::Load("data/FILE/background.json");

	LoadEnemy("data/FILE/enemy.json");
	LoadPlayer("data/FILE/player.json");

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CGame::Uninit()
{
	CManager::GetSound()->Stop();

	CManager::SetNowScore(m_pScore->GetScore());

	CObject::Release();

	// リリースはリリースオールでやってある
	m_pScore = nullptr;
	m_pPlayer3D = nullptr;
	m_pLockOnUIManager = nullptr;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CGame::Update()
{
	m_time++;

	// エネミーの最大数
	int nIndex = EnemyList["INDEX"];
	// 位置
	D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
	// 大きさ
	D3DXVECTOR3 move(0.0f, 0.0f, 0.0f);
	// 角度
	D3DXVECTOR3 rot(0.0f, 0.0f, 0.0f);
	// 体力
	int Life = 0;
	// フレーム
	int frame = 0;
	// enemytype
	CEnemy3D::ENEMY_TYPE m_enemyType;

	for (int nCntEnemy = 0; nCntEnemy < nIndex; nCntEnemy++)
	{// タグ付け
		std::string name = "ENEMY";
		std::string Number = std::to_string(nCntEnemy);
		name += Number;

		pos = D3DXVECTOR3(EnemyList[name]["POS"]["X"], EnemyList[name]["POS"]["Y"], EnemyList[name]["POS"]["Z"]);
		move = D3DXVECTOR3(EnemyList[name]["MOVE"]["X"], EnemyList[name]["MOVE"]["Y"], EnemyList[name]["MOVE"]["Z"]);
		rot = D3DXVECTOR3(EnemyList[name]["ROT"]["X"], EnemyList[name]["ROT"]["Y"], EnemyList[name]["ROT"]["Z"]);
		Life = EnemyList[name]["LIFE"];
		m_enemyType = EnemyList[name]["TYPE"];
		frame = EnemyList[name]["FRAME"];

		if (frame == m_time)
		{
			CEnemy3D *pEnemy = CEnemy3D::Create(m_enemyType);
			pEnemy->SetPos(pos);
			pEnemy->SetMove(move);
			pEnemy->SetRot(rot);
			pEnemy->SetLife(Life);
		}
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CGame* CGame::Create()
{
	CGame *pGame;
	pGame = new CGame;

	if (pGame != nullptr)
	{
		pGame->Init();
	}
	else
	{
		assert(false);
	}

	return pGame;
}

//--------------------------------------------------
// 読み込み
//--------------------------------------------------
void CGame::LoadEnemy(const char *pFdata)
{
	// ファイルオープン
	std::ifstream ifs(pFdata);

	if (ifs)
	{// 開けたら
		ifs >> EnemyList;
	}
}

//--------------------------------------------------
// 読み込み
//--------------------------------------------------
void CGame::LoadPlayer(const char *pFdata)
{
	// ファイルオープン
	std::ifstream ifs(pFdata);

	if (ifs)
	{// 開けたら
		ifs >> PlayerList;

		// 位置
		D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
		// 大きさ
		D3DXVECTOR3 move(0.0f, 0.0f, 0.0f);
		// 角度
		D3DXVECTOR3 rot(0.0f, 0.0f, 0.0f);
		// 体力
		int Life = 0;

		std::string name = "PLAYER";

		pos = D3DXVECTOR3(PlayerList[name]["POS"]["X"], PlayerList[name]["POS"]["Y"], PlayerList[name]["POS"]["Z"]);
		move = D3DXVECTOR3(PlayerList[name]["MOVE"]["X"], PlayerList[name]["MOVE"]["Y"], PlayerList[name]["MOVE"]["Z"]);
		rot = D3DXVECTOR3(PlayerList[name]["ROT"]["X"], PlayerList[name]["ROT"]["Y"], PlayerList[name]["ROT"]["Z"]);
		Life = PlayerList[name]["LIFE"];

		m_pPlayer3D = CPlayer3D::Create();
		m_pPlayer3D->SetPos(pos);
		m_pPlayer3D->SetMove(move);
		m_pPlayer3D->SetRot(rot);
		m_pPlayer3D->SetLife(Life);
		m_pPlayer3D->SetModel("data/MODEL/X_wing.x");
	}
}
