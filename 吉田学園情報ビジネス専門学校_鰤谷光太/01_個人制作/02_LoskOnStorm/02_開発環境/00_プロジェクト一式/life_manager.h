//==================================================
// life_manager.h
// Author: Buriya Kota
//==================================================
#ifndef _LIFE_MANAGER_H_
#define _LIFE_MANAGER_H_

//**************************************************
// インクルード
//**************************************************
#include "object.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CLife;

//**************************************************
// クラス
//**************************************************
class CLifeManager : public CObject
{
public:
	static const int MAX_LIFE = 3;

public:
	explicit CLifeManager(int nPriority = PRIORITY_EFFECT);
	~CLifeManager() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override {}
	void Draw() override {}

	void SetLife(int nLife) { m_nLife = nLife; }
	static int GetLife() { return m_nLife; }
	static void SubLife(int nValue);

	static CLifeManager *Create(int nLife);

private:
	// Number型の配列
	static CLife *m_pLife[MAX_LIFE];
	// 現在のライフ
	static int m_nLife;
};

#endif	// _LIFE_MANAGER_H_