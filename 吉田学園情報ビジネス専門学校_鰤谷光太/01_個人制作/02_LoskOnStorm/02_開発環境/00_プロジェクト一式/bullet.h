//==================================================
// bullet.h
// Author: Buriya Kota
//==================================================
#ifndef _BULLET_H_
#define _BULLET_H_

//**************************************************
// インクルード
//**************************************************
#include "polygon3D.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CBullet : public CPolygon3D
{
public:
	explicit CBullet(int nPriority = PRIORITY_BULLET);
	~CBullet() override;

	virtual HRESULT Init() override;
	virtual void Update() override;

	static CBullet *Create(const D3DXVECTOR3 move, const D3DXVECTOR3 size);

private:
	void BulletMove_();
	void CollisionEnemy_();
	void CollisionBoss_();

private:
	// 終了までの時間
	int m_time;;

};

#endif	// _BULLET_H_