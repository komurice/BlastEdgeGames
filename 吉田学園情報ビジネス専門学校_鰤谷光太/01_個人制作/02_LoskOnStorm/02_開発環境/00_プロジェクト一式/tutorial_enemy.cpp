//==================================================
// tutorial_enemy.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "tutorial_enemy.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CTutorialEnemy::CTutorialEnemy()
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CTutorialEnemy::~CTutorialEnemy()
{
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CTutorialEnemy::Update()
{
	CEnemy3D::Update();

	MoveTutorialEnemy_();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CTutorialEnemy *CTutorialEnemy::Create()
{
	CTutorialEnemy *pTutorialEnemy;
	pTutorialEnemy = new CTutorialEnemy;

	if (pTutorialEnemy != nullptr)
	{
		pTutorialEnemy->Init();
	}
	else
	{
		assert(false);
	}

	return pTutorialEnemy;
}

//--------------------------------------------------
// 動き
//--------------------------------------------------
void CTutorialEnemy::MoveTutorialEnemy_()
{
	D3DXVECTOR3 move = GetMove();
	SetMove(move);

	MovePos(move);

	D3DXVECTOR3 pos = GetPos();

	if (pos.x < -1200.0f)
	{
		Uninit();
	}
}
