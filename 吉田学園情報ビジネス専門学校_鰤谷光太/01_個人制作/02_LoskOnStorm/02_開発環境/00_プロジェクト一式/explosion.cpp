//==================================================
// explosion.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "object.h"
#include "explosion.h"
#include "manager.h"
#include "input_keyboard.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CExplosion::CExplosion()
{
	// アニメーションカウンター
	m_nCounterAnim = 0;
	// アニメーションパターンNo.
	m_nPatternAnim = 0;
	// タイプ設定
	SetType(TYPE_EXPLOSION);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CExplosion::~CExplosion()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CExplosion::Init()
{
	// アニメーションカウンター
	m_nCounterAnim = 0;
	// アニメーションパターンNo.
	m_nPatternAnim = 0;

	// 初期化
	CPolygon3D::Init();
	// テクスチャの設定
	CPolygon3D::SetTexture(CTexture::TEXTURE_EXPLOSION);
	// アニメーションの初期化
	CPolygon3D::AnimTexture(m_nPatternAnim, 0);

	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CExplosion::Update()
{
	CPolygon3D::Update();

	m_nCounterAnim++;

	if ((m_nCounterAnim % 6) == 0)
	{// アニメーションの計算
		CPolygon3D::AnimTexture(m_nPatternAnim, EXPLOSION_ANIM);
		m_nPatternAnim++;
	}

	if (m_nPatternAnim >= EXPLOSION_ANIM)
	{
		Uninit();
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CExplosion *CExplosion::Create(const D3DXVECTOR3 size)
{
	CExplosion *pExplosion;
	pExplosion = new CExplosion;

	if (pExplosion != nullptr)
	{
		pExplosion->Init();
		pExplosion->SetSize(size);
		pExplosion->SetBillboard(true);
	}
	else
	{
		assert(false);
	}

	return pExplosion;
}
