//==================================================
// score.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "life.h"
#include "life_manager.h"

//**************************************************
// 静的メンバ変数
//**************************************************
CLife *CLifeManager::m_pLife[MAX_LIFE] = {};
int CLifeManager::m_nLife = 0;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CLifeManager::CLifeManager(int nPriority /* =4 */) : CObject(nPriority)
{
	m_nLife = 0;
	SetType(TYPE_LIFE);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CLifeManager::~CLifeManager()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CLifeManager::Init()
{
	for (int nCnt = 0; nCnt < MAX_LIFE; nCnt++)
	{
		m_pLife[nCnt] = CLife::Create(D3DXVECTOR3(50.0f * nCnt + 60.0f, 100.0f, 0.0f), D3DXVECTOR3(60.0f, 50.0f, 0.0f));
	}

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CLifeManager::Uninit()
{
	for (int nCnt = 0; nCnt < MAX_LIFE; nCnt++)
	{
		if (m_pLife[nCnt] == nullptr)
		{
			continue;
		}

		m_pLife[nCnt]->Uninit();
	}

	CObject::Release();
}

//--------------------------------------------------
// ライフの減算
//--------------------------------------------------
void CLifeManager::SubLife(int nValue)
{
	m_nLife -= nValue;

	for (int nCntLife = 0; nCntLife < MAX_LIFE; nCntLife++)
	{
		if (nCntLife >= m_nLife)
		{
			m_pLife[nCntLife]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
		}
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CLifeManager *CLifeManager::Create(int nLife)
{
	CLifeManager *pLifeManager;
	pLifeManager = new CLifeManager;

	if (pLifeManager != nullptr)
	{
		pLifeManager->Init();
		pLifeManager->SetLife(nLife);
	}
	else
	{
		assert(false);
	}

	return pLifeManager;
}
