//==================================================
// boss.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "game.h"

#include "enemy_bullet.h"
#include "boss.h"
#include "player3D.h"
#include "sound.h"

#include "utility.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CBoss::CBoss()
{
	// タイプ設定
	SetType(TYPE_BOSS);
	SetEnemyType(ENEMY_TYPE_BOSS);

	m_nCntFrame = 0;
	m_nShotTime = 0;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CBoss::~CBoss()
{
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CBoss::Update()
{
	CEnemy3D::Update();

	MoveBoss_();
	ShotBullet_();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CBoss * CBoss::Create()
{
	CBoss *pBoss;
	pBoss = new CBoss;

	if (pBoss != nullptr)
	{
		pBoss->Init();
	}
	else
	{
		assert(false);
	}

	return pBoss;
}

//--------------------------------------------------
// ボスの動き
//--------------------------------------------------
void CBoss::MoveBoss_()
{
	D3DXVECTOR3 move = GetMove();
	SetMove(move);

	m_nCntFrame++;

	// 左から着て一定フレームで止まり高さ上昇して上に消えていく
	if (m_nCntFrame == 120)
	{
		move = D3DXVECTOR3(-5.0f, 0.0f, 0.0f);
		SetMove(move);
	}
	
	if (GetPos().x < -500.0f)
	{
		move = D3DXVECTOR3(5.0f, 0.0f, 0.0f);
		SetMove(move);
	}
	else if (GetPos().x > 500.0f)
	{
		move = D3DXVECTOR3(-5.0f, 0.0f, 0.0f);
		SetMove(move);
	}

	MovePos(move);
}

//--------------------------------------------------
// ボスの動き
//--------------------------------------------------
void CBoss::ShotBullet_()
{
	D3DXVECTOR3 myPos = GetPos();
	D3DXVECTOR3 targetPos = CGame::GetPlayer3D()->GetPos();

	m_nShotTime++;

	if (m_nShotTime == 60)
	{
		// サウンド
		CManager::GetSound()->Play(CSound::LABEL_SE_SHOT);

		CEnemyBullet::Create(D3DXVECTOR3(0.0f, 0.0f, -10.0f), D3DXVECTOR3(40.0f, 40.0f, 0.0f))->SetPos(myPos + D3DXVECTOR3(50.0f, 0.0f, 0.0f));
		CEnemyBullet::Create(D3DXVECTOR3(0.0f, 0.0f, -10.0f), D3DXVECTOR3(40.0f, 40.0f, 0.0f))->SetPos(myPos);
		CEnemyBullet::Create(D3DXVECTOR3(0.0f, 0.0f, -10.0f), D3DXVECTOR3(40.0f, 40.0f, 0.0f))->SetPos(myPos + D3DXVECTOR3(-50.0f, 0.0f, 0.0f));
	}
	else if (m_nShotTime == 180)
	{
		// サウンド
		CManager::GetSound()->Play(CSound::LABEL_SE_SHOT);

		D3DXVECTOR3 vec = targetPos - myPos;
		D3DXVec3Normalize(&vec, &vec);

		D3DXVECTOR3 move(0.0f, 0.0f, 0.0f);

		move += vec * 15.0f;

		CEnemyBullet::Create(move, D3DXVECTOR3(40.0f, 40.0f, 0.0f))->SetPos(myPos + D3DXVECTOR3(100.0f, 0.0f, 0.0f));
		CEnemyBullet::Create(move, D3DXVECTOR3(40.0f, 40.0f, 0.0f))->SetPos(myPos + D3DXVECTOR3(-100.0f, 0.0f, 0.0f));

		m_nShotTime = 0;
	}
}
