//==================================================
// meshfilde.h
// Author: Buriya Kota
//==================================================
#ifndef _MESHFILDE_H_
#define _MESHFILDE_H_

//**************************************************
// インクルード
//**************************************************
#include "object.h"
#include "texture.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// クラス
//**************************************************
class CMeshFilde : public CObject
{
public:
	static const float MESHFILDE_SIZE;
	static const float MESH_X;
	static const float MESH_Z;
	static const float MESH_LENGTH;

public:
	explicit CMeshFilde(int nPriority = PRIORITY_BG);
	~CMeshFilde() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	void SetPos(const D3DXVECTOR3& pos) { m_pos = pos; }
	void SetSize(const D3DXVECTOR3& size) { m_size = size; }
	void SetTexture(CTexture::TEXTURE texture) { m_texture = texture; }	// テクスチャの設定

	const D3DXVECTOR3& GetPos() const { return m_pos; }
	const D3DXVECTOR3& GetSize() const { return m_size; }

	static CMeshFilde *Create();

private:
	// 位置
	D3DXVECTOR3 m_pos;
	// 移動量
	D3DXVECTOR3 m_move;
	// 回転
	D3DXVECTOR3 m_rot;
	// サイズ
	D3DXVECTOR3 m_size;
	int m_maxVtx;
	int m_maxIdx;
	int m_maxPolygon;
	// ワールドマトリックス
	D3DXMATRIX m_mtxWorld;
	// テクスチャの列挙型
	CTexture::TEXTURE m_texture;
	// 頂点バッファへのポインタ
	LPDIRECT3DVERTEXBUFFER9 m_pVtxBuff;
	// インデックスバッファへのポインタ
	LPDIRECT3DINDEXBUFFER9 m_pIdxBuff;

};

#endif	// _MESHFILDE_H_