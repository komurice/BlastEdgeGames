//==================================================
// tridoron.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "game.h"
#include "player3D.h"
#include "tridoron.h"
#include "add_lockon_item.h"

#include "sound.h"
#include "enemy_bullet.h"

#include "utility.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CTridoron::CTridoron() : m_nCntFrame(0)
{
	SetEnemyType(ENEMY_TYPE_TRIDORON);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CTridoron::~CTridoron()
{
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CTridoron::Update()
{
	CEnemy3D::Update();

	MoveTridoron_();
	ShotBullet_();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CTridoron * CTridoron::Create()
{
	CTridoron *pTridoron;
	pTridoron = new CTridoron;

	if (pTridoron != nullptr)
	{
		pTridoron->Init();
	}
	else
	{
		assert(false);
	}

	return pTridoron;
}

//--------------------------------------------------
// トライドロンの動き
//--------------------------------------------------
void CTridoron::MoveTridoron_()
{
	D3DXVECTOR3 move = GetMove();
	SetMove(move);

	m_nCntFrame++;

	// 左から着て一定フレームで止まり高さ上昇して上に消えていく
	if (m_nCntFrame == 120)
	{
		move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		SetMove(move);
	}
	else if (m_nCntFrame > 240)
	{
		move.z += 0.5f;
		SetMove(move);
	}

	MovePos(move);

	D3DXVECTOR3 pos = GetPos();

	if (pos.z > 1100.0f)
	{
		Uninit();
	}
}

//--------------------------------------------------
// ボスの動き
//--------------------------------------------------
void CTridoron::ShotBullet_()
{
	D3DXVECTOR3 myPos = GetPos();
	D3DXVECTOR3 targetPos = CGame::GetPlayer3D()->GetPos();

	m_nShotTime++;

	if (m_nShotTime == 80)
	{
		// サウンド
		CManager::GetSound()->Play(CSound::LABEL_SE_SHOT);

		D3DXVECTOR3 vec = targetPos - myPos;
		D3DXVec3Normalize(&vec, &vec);

		D3DXVECTOR3 move(0.0f, 0.0f, 0.0f);

		move += vec * 15.0f;

		CEnemyBullet::Create(move, D3DXVECTOR3(40.0f, 40.0f, 0.0f))->SetPos(myPos);

		m_nShotTime = 0;
	}
}

