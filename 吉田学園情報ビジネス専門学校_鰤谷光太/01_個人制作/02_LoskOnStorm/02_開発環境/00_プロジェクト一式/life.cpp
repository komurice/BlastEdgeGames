//==================================================
// life.h
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "life.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CLife::CLife()
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CLife::~CLife()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CLife::Init()
{
	CObject2D::Init();

	SetTexture(CTexture::TEXTURE_LIFE);

	return S_OK;
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CLife *CLife::Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 size)
{
	CLife *pLife;
	pLife = new CLife;

	if (pLife != nullptr)
	{
		pLife->Init();
		pLife->SetPos(pos);
		pLife->SetSize(size);
	}
	else
	{
		assert(false);
	}

	return pLife;
}
