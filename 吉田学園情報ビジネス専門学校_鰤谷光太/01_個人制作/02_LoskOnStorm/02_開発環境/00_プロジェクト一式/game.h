//==================================================
// game.h
// Author: Buriya Kota
//==================================================
#ifndef _GAME_H_
#define _GAME_H_

//**************************************************
// インクルード
//**************************************************
#include "game_mode.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CScore;
class CLockOnUIManager;
class CPlayer3D;
class CPause;

//**************************************************
// クラス
//**************************************************
class CGame : public CGameMode
{
public:
	CGame();
	~CGame() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override {}

	// エネミーの読み込み
	void LoadEnemy(const char *pFdata);
	void LoadPlayer(const char *pFdata);

	// プレイヤーの情報の取得
	static CPlayer3D* GetPlayer3D() { return m_pPlayer3D; }
	static CScore* GetScore() { return m_pScore; }
	static CLockOnUIManager* GetLockOnUIManager() { return m_pLockOnUIManager; }
	static CPause* GetPause() { return m_pPause; }

	// フレームの設定
	int GetFrame() { return m_time; }

	static CGame *Create();

private:
	int m_time;		// ゲーム開始からの時間
	
	static CPlayer3D *m_pPlayer3D;
	static CScore *m_pScore;
	static CLockOnUIManager *m_pLockOnUIManager;
	static CPause *m_pPause;

};

#endif	// _GAME_H_