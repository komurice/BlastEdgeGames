//==================================================
// snake.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include "manager.h"
#include "snake.h"

#include "utility.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CSnake::CSnake()
{
	SetEnemyType(ENEMY_TYPE_SNAKE);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CSnake::~CSnake()
{
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CSnake::Update()
{
	CEnemy3D::Update();

	MoveSnake_();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CSnake * CSnake::Create()
{
	CSnake *pSnake;
	pSnake = new CSnake;

	if (pSnake != nullptr)
	{
		pSnake->Init();
	}
	else
	{
		assert(false);
	}

	return pSnake;
}

//--------------------------------------------------
// スネークの動き
//--------------------------------------------------
void CSnake::MoveSnake_()
{
	D3DXVECTOR3 move = GetMove();
	SetMove(move);

	m_nCntFrame++;

	if (m_nCntFrame > 60)
	{
		move.z += 1.0f;
		SetMove(move);
	}

	MovePos(move);

	D3DXVECTOR3 pos = GetPos();

	if (pos.z > 1000.0f)
	{
		Uninit();
	}
}
