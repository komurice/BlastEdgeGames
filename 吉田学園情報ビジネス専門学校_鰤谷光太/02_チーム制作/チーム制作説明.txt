//==============================================================================
//
// チーム制作 作品説明資料
//
// Author : Buriya Kota
//
// 最終更新日 : 2023/03/09
//==============================================================================


◆01_RUNFOX
使用言語 : C++
開発期間 : 2022年11月(1か月)
ジャンル : 3Dアクション

担当箇所
メイン：[debug_proc.cpp]  [meshfield.cpp]  [light.cpp]  [renderer.cpp]  [main.cpp]  [manager.cpp]  [utility.cpp]
  
スカイボックス：[Skyfield.cpp]
 
入力処理：[input.cpp]  [input_joypad.cpp]  [input_keyboard.cpp] 

カメラの制御：[camera.cpp]

オブジェクト：[object.cpp]  [object2D.cpp]  [object3D.cpp]  [model.cpp]

当たり判定：[building.cpp] の Collision（当たり判定）処理

ステンシルバッファを使ったプレイヤーのシルエット表示：
[silhouette.cpp]
[motion_model3D.cpp] の Draw（描画）処理
[building.cpp] の Draw（描画）処理
[meshfield.cpp] の Draw（描画）処理


◆作品説明
障害物を乗り越えてゴールを目指すアクションゲームです

◆工夫した点・苦労した点
自分が担当した個所では、ステンシルバッファを使用したプレイヤーのシルエット表示を実装しました。
それぞれの描画順やどのピクセルに数値を設定し表示させるかなど、そこを考えるのに時間がかかりました。

◆反省点
チーム制作なので、それぞれのタスク管理がしっかりできていなかったので、そこで作業ペースが遅れてしまうことが
チーム全体でありました。また、個人の反省では、自分の担当している個所に時間をかけすぎてしまったことです。

◆その他
https://youtu.be/ah_ZIKWy7bY


