//==================================================
// camera.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "manager.h"
#include "input_keyboard.h"
#include "input.h"

#include "camera.h"
#include "game.h"
#include "stage.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CCamera::CCamera()
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CCamera::~CCamera()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
void CCamera::Init(void)
{
	// 視点・注視点・上方向を設定する
	m_posV = D3DXVECTOR3(0.0f, 0.0f, -750.0f);		// 視点
	m_posR = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// 注視点
	m_posVDest = D3DXVECTOR3(0.0f, 100.0f, -100.0f);	// 目的の視点
	m_posRDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f);			// 目的の注視点
	m_vecU = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

	float fDisX = m_posR.x - m_posV.x;
	float fDisY = m_posR.y - m_posV.y;
	float fDisZ = m_posR.z - m_posV.z;

	// 角度の初期化
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	// 初期の角度の計算
	m_rot.x = atan2f(fDisZ, fDisY);

	m_rot.x += 1.0f;
	// 視点から注視点までの距離を算出
	m_fDis = sqrtf((fDisX * fDisX) + (fDisZ * fDisZ));
	m_fDis = sqrtf((m_fDis * m_fDis) + (fDisY * fDisY));
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CCamera::Uninit(void)
{
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CCamera::Update(void)
{
	if ((CGame::GetStage()!= nullptr)&&CManager::GetGameMode() == CManager::MODE_GAME)
	{
		const D3DXVECTOR3 Pos = CGame::GetStage()->GetPlayer()->GetPos();
		const D3DXVECTOR3 Rot = CGame::GetStage()->GetPlayer()->GetRot();
		m_posRDest.x = Pos.x + sinf(Rot.y - D3DX_PI) *10.0f;         //目的の値
		m_posRDest.y = Pos.y + cosf(Rot.y - D3DX_PI) *10.0f;         //目的の値
		m_posRDest.z = Pos.z + cosf(Rot.y - D3DX_PI) *10.0f;

		m_posVDest.x = Pos.x - sinf(m_rot.y)*m_fDis;          //目的の値
		m_posVDest.y = Pos.y - sinf(m_rot.x - D3DX_PI)*m_fDis;          //目的の値
		m_posVDest.z = Pos.z - cosf(m_rot.y)*m_fDis;

		m_posR.x += (m_posRDest.x - m_posR.x) * 0.1f;
		m_posR.y += (m_posRDest.y - m_posR.y) * 0.1f;
		m_posR.z += (m_posRDest.z - m_posR.z) * 0.1f;

		m_posV.x += (m_posVDest.x - m_posV.x) * 0.1f;
		m_posV.y += (m_posVDest.y - m_posV.y) * 0.1f;
		m_posV.z += (m_posVDest.z - m_posV.z) * 0.1f;

		//正規化
		if (m_rot.y > D3DX_PI)
		{
			m_rot.y -= D3DX_PI*2.0f;
		}
		if (m_rot.y < -D3DX_PI)
		{
			m_rot.y += D3DX_PI*2.0f;
		}

		//正規化
		if (m_rot.x > D3DX_PI)
		{
			m_rot.x -= D3DX_PI*2.0f;
		}
		if (m_rot.x < -D3DX_PI)
		{
			m_rot.x += D3DX_PI*2.0f;
		}
	}
	else if(CManager::GetGameMode() == CManager::MODE_TITLE)
	{// タイトル用のカメラ
	 // 計算用マトリックス
		D3DXMATRIX mtx;
		// マトリックスの生成
		D3DXMatrixIdentity(&mtx);

		// ヨー、ピッチ、ロールを指定してマトリックスを作成
		D3DXMatrixRotationYawPitchRoll(&mtx, D3DXToRadian(0.25f), 0.0f, 0.0f);		// 回転
		D3DXVec3TransformCoord(&m_posV, &m_posV, &mtx);

		m_posV += m_posR;
		m_posV.y = 300.0f;
	}
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CCamera::Set()
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

	// ビューマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxView);

	// ビューマトリックスの作成
	D3DXMatrixLookAtLH(&m_mtxView,
		&m_posV,
		&m_posR,
		&m_vecU);

	// ビューマトリックスの設定
	pDevice->SetTransform(D3DTS_VIEW, &m_mtxView);

	// プロジェクションマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxProjection);

	// プロジェクションマトリックスの作成
	D3DXMatrixPerspectiveFovLH(&m_mtxProjection,
		D3DXToRadian(45.0f),								// 視野角
		(float)CManager::SCREEN_WIDTH / (float)CManager::SCREEN_HEIGHT,			// アスペクト比
		10.0f,												// ニア
		10000.0f);											// ファー

	// プロジェクションマトリックスの設定
	pDevice->SetTransform(D3DTS_PROJECTION, &m_mtxProjection);
}
