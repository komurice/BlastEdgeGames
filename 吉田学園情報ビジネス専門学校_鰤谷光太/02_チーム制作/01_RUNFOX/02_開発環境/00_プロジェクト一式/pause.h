//==================================================
// pause.h
// Author: tutida ryousei
//==================================================
#ifndef _PAUSE_H_
#define	_PAUSE_H_

class CObject2D;

class CPause
{
public:
	enum PAUSE
	{
		PAUSE_BG = 0,		// ポースの背景
		PAUSE_CONTINUE,		// 続ける
		PAUSE_TITLE,		// タイトルに戻る
		PAUSE_MAX,
		PAUSE_NONE,
	};

	CPause();
	~CPause();

	void Init();
	void Update();

	static CPause *Create();

	void In_Pause();	// ポーズ中の処理
	void Select();		// 選択処理

	// ゲッター
	bool GetPause() { return m_bPause; }

private:
	int m_nSelect;		// 選択
	float m_fAlpha;		// アルファ値
	bool m_bPause;		// ポーズ中か
	CObject2D *m_pObject2D[PAUSE_MAX];	// オブジェクト2Dの情報
};

#endif // !_PAUSE_H_
