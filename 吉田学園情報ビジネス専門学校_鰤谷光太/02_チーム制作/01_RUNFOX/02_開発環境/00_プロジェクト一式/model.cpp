//==================================================
// model.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"

#include "player3D.h"
#include "model.h"
#include "light.h"

//**************************************************
// 静的メンバ変数
//**************************************************
CModel::MODEL CModel::m_model[MAX_MODEL] = {};

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CModel::CModel()
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CModel::~CModel()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CModel::Init(int nType)
{
	m_nType = nType;

	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_scale = D3DXVECTOR3(3.0f, 3.0f, 3.0f);
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CModel::Uninit()
{
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CModel::Update()
{
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CModel::Draw()
{
	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();
	// 計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans, mtxScale, mtxParent;
	// 現在のマテリアル保存用
	D3DMATERIAL9 matDef;
	// マテリアルデータへのポインタ
	D3DXMATERIAL *pMat;

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxWorld);

	// 行列拡縮関数
	D3DXMatrixScaling(&mtxScale, m_scale.x, m_scale.y, m_scale.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxScale);

	// 向きを反映
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);

	// 位置を反映
	D3DXMatrixTranslation(&mtxTrans, m_pos.x, m_pos.y, m_pos.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);

	if (m_pParent != nullptr)
	{// 親が設定されていなかったら親になる
		mtxParent = m_pParent->GetMtxWorld();
	}
	else
	{// 親子付け
		pDevice->GetTransform(D3DTS_WORLD, &mtxParent);
	}

	// マトリックスの掛け算
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxParent);

	// 影の描画
	DrawShadow_();

	// ワールドマトリックスの設定
	pDevice->SetTransform(D3DTS_WORLD, &m_mtxWorld);

	// 現在のマテリアル保持
	pDevice->GetMaterial(&matDef);

	// マテリアルデータへのポインタを取得
	pMat = (D3DXMATERIAL*)m_model[m_nType].buffMat->GetBufferPointer();

	for (int i = 0; i < (int)m_model[m_nType].numMat; i++)
	{
		pMat[i].MatD3D.Ambient = pMat[i].MatD3D.Diffuse;
		// マテリアルの設定
		pDevice->SetMaterial(&pMat[i].MatD3D);

		// テクスチャの設定
		pDevice->SetTexture(0, m_model[m_nType].texture[i]);

		// モデルパーツの描画
		m_model[m_nType].mesh->DrawSubset(i);
	}

	// 保存していたマテリアルを戻す
	pDevice->SetMaterial(&matDef);

	// テクスチャの設定
	pDevice->SetTexture(0, NULL);
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CModel * CModel::Create(int nType)
{
	CModel *pModel;
	pModel = new CModel;

	if (pModel != nullptr)
	{
		pModel->Init(nType);
	}
	else
	{
		assert(false);
	}

	return pModel;
}

//--------------------------------------------------
// 読み込み
//--------------------------------------------------
void CModel::LoadModel(const char *pFile,int nNum)
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

	// Xファイルの読み込み
	D3DXLoadMeshFromX(pFile,
		D3DXMESH_SYSTEMMEM,
		pDevice,
		NULL,
		&m_model[nNum].buffMat,
		NULL,
		&m_model[nNum].numMat,
		&m_model[nNum].mesh);

	// バッファの先頭ポインタをD3DXMATERIALにキャストして取得
	D3DXMATERIAL *pMat = (D3DXMATERIAL*)m_model[nNum].buffMat->GetBufferPointer();

	// 各メッシュのマテリアル情報を取得する
	for (int i = 0; i < (int)m_model[nNum].numMat; i++)
	{
		m_model[nNum].texture[i] = NULL;

		if (pMat[i].pTextureFilename != NULL)
		{// マテリアルで設定されているテクスチャ読み込み
			D3DXCreateTextureFromFileA(pDevice,
				pMat[i].pTextureFilename,
				&m_model[nNum].texture[i]);
		}
	}
}

//--------------------------------------------------
// 解放
//--------------------------------------------------
void CModel::UnLoadModel()
{
	for (int nCnt = 0; nCnt < MAX_MODEL; nCnt++)
	{
		for (int i = 0; i < (int)m_model[nCnt].numMat; i++)
		{
			if (m_model[nCnt].texture[i] != nullptr)
			{// テクスチャの解放
				m_model[nCnt].texture[i]->Release();
				m_model[nCnt].texture[i] = nullptr;
			}
		}

		if (m_model[nCnt].mesh != nullptr)
		{// メッシュの破棄
			m_model[nCnt].mesh->Release();
			m_model[nCnt].mesh = nullptr;
		}

		if (m_model[nCnt].buffMat != nullptr)
		{// マテリアルの解放
			m_model[nCnt].buffMat->Release();
			m_model[nCnt].buffMat = nullptr;
		}
	}
}

//--------------------------------------------------
// 影の描画
//--------------------------------------------------
void CModel::DrawShadow_()
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();
	// 現在のマテリアル保存用
	D3DMATERIAL9 matDef;
	// マテリアルデータへのポインタ
	D3DXMATERIAL *pMat;
	D3DXMATRIX mtxShadow;
	D3DXPLANE planeField;
	D3DXVECTOR4 vecLight;
	D3DXVECTOR3 pos, normal;

	// アンビエントを無効にする
	CManager::GetRenderer()->GetDevice()->SetRenderState(D3DRS_AMBIENT, 0);

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&mtxShadow);

	// ライトを取ってくる
	CLight *pLight = CManager::GetLight();

	D3DXVECTOR3 vecDir = pLight->Get().Direction;

	vecLight = D3DXVECTOR4(-vecDir.x, -vecDir.y, -vecDir.z, 0.0f);

	pos = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	normal = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

	// 
	D3DXPlaneFromPointNormal(&planeField, &pos, &normal);
	// 
	D3DXMatrixShadow(&mtxShadow, &vecLight, &planeField);

	// 行列の掛け算
	D3DXMatrixMultiply(&mtxShadow, &m_mtxWorld, &mtxShadow);

	// ワールドマトリックスの設定
	pDevice->SetTransform(D3DTS_WORLD, &mtxShadow);

	// 現在のマテリアル保持
	pDevice->GetMaterial(&matDef);

	// マテリアルデータへのポインタを取得
	pMat = (D3DXMATERIAL*)m_model[m_nType].buffMat->GetBufferPointer();

	// テクスチャの設定
	pDevice->SetTexture(0, NULL);

	for (int i = 0; i < (int)m_model[m_nType].numMat; i++)
	{
		// マテリアル情報の設定
		D3DMATERIAL9 matD3D = pMat[i].MatD3D;

		// 引数を色に設定
		matD3D.Diffuse = D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f);
		matD3D.Emissive = D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f);

		// マテリアルの設定
		pDevice->SetMaterial(&matD3D);

		// モデルパーツの描画
		m_model[m_nType].mesh->DrawSubset(i);
	}

	// 保存していたマテリアルを戻す
	pDevice->SetMaterial(&matDef);
	// アンビエントをもとに戻す
	CManager::GetRenderer()->GetDevice()->SetRenderState(D3DRS_AMBIENT, 0xffffffff);
}
