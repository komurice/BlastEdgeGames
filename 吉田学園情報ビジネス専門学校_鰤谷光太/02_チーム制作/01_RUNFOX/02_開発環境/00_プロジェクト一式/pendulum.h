//==================================================
// pendulum.h
// Author: tutida ryousei
//==================================================
#ifndef _PENBULUM_H_
#define	_PENBULUM_H_

#include"building.h"

class CPendulum : public CBuilding
{
public:
	CPendulum();
	~CPendulum() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	void Hit() override;

	static CPendulum *Create(D3DXVECTOR3 pos, D3DXVECTOR3 destrot, float coefficient, int type);
	void Move();	// �ړ�����

	// �Z�b�^�[
	void SetDestRot(D3DXVECTOR3 destrot) { m_DestRot = destrot; }
	void SetCoefficient(float coefficient) { m_fCoefficient = coefficient; }
	void SetType(int type) { m_nType = type; }
	
private:
	int m_nType;			// ���
	float m_fCoefficient;	// �����A�����W��
	D3DXVECTOR3 m_Rot;		// �p�x
	D3DXVECTOR3 m_DestRot;	// �ړI�̊p�x(�U�蕝)
};

#endif // !_PENBULUM_H_
