//============================
//
// リザルト画面のヘッダー
// Author:hamada ryuuga
//
//============================
#ifndef _RESULT_H_		//このマクロが定義されてなかったら
#define _RESULT_H_		//2重インクルード防止のマクロ定義

#include "main.h"
#include "object.h"
#include "object2d.h"

class CSound;

class CResult :public CObject
{
public:
	CResult(int nPriority = 1);
	~CResult();
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;	

	static CResult*Create();
	static void SetScore(int nScore) { m_Score = nScore; };
	static int GetMyScore() { return m_Score; };

private:
	void SetBG();					//背景の配置
	void SetResultLogo();			//”Result”と書かれたロゴを配置する

	void GetScore();

private:
	static	std::string m_Name[5];
	int m_addX;
	int m_addY;
	int m_timer;
	bool Sizcontroller;
	bool m_finished;
	bool m_Pop;
	static CSound * m_pSound;

	static int m_Score;

};

#endif