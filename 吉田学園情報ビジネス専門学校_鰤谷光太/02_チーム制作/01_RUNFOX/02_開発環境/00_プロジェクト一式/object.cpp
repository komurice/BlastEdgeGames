//==================================================
// object.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <time.h>
#include <assert.h>

#include "object.h"
#include "object2D.h"
#include "renderer.h"
#include "pause.h"
#include "manager.h"

//**************************************************
// 静的メンバ変数
//**************************************************
int CObject::m_nNumAll = 0;
CObject *CObject::m_pTop[] = {};
CObject *CObject::m_pCurrent[] = {};

//--------------------------------------------------
// プライオリティを使ったコンストラクタ
//--------------------------------------------------
CObject::CObject(int nPriority /* PRIORITY_3 */) : m_pPrev(nullptr), m_pNext(nullptr)
{

	m_type = TYPE_NONE;
	if (m_pTop[nPriority] == nullptr)
	{// Topがnullptrの時
		m_pTop[nPriority] = this;
	}
	else
	{// Topがすでに生成されているとき
		m_pCurrent[nPriority]->m_pNext = this;
		this->m_pPrev = m_pCurrent[nPriority];
	}

	m_pCurrent[nPriority] = this;

	m_nPriority = nPriority;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CObject::~CObject()
{
}

//--------------------------------------------------
// すべての破棄
//--------------------------------------------------
void CObject::ReleaseAll()
{
	for (int nCnt = 0; nCnt < MAX_PRIO; nCnt++)
	{
		CObject *pCurrentObj = m_pTop[nCnt];

		while (pCurrentObj != nullptr)
		{
			CObject *pObjNext = pCurrentObj->m_pNext;

			if (!pCurrentObj->IsDeleted())
			{
				pCurrentObj->Uninit();
			}

			pCurrentObj = pObjNext;
		}
	}

	//deleted == true を delete & null代入
	for (int nCnt = 0; nCnt < MAX_PRIO; nCnt++)
	{
		CObject *pCurrentObj = m_pTop[nCnt];

		while (pCurrentObj != nullptr)
		{
			CObject *pObjNext = pCurrentObj->m_pNext;

			if (pCurrentObj->IsDeleted())
			{
				pCurrentObj->Release();
			}
			else
			{
				//assert(false);
			}

			pCurrentObj = pObjNext;
		}

		m_pTop[nCnt] = nullptr;
		m_pCurrent[nCnt] = nullptr;
	}
}

//--------------------------------------------------
// すべての更新
//--------------------------------------------------
void CObject::UpdateAll()
{
	bool bPause = CManager::GetPause()->GetPause();

	if (!bPause)
	{
		for (int nCnt = 0; nCnt < MAX_PRIO; nCnt++)
		{
			CObject *pCurrentObj = m_pTop[nCnt];

			while (pCurrentObj != nullptr)
			{
				CObject *pObjNext = pCurrentObj->m_pNext;

				if (!pCurrentObj->IsDeleted())
				{
					pCurrentObj->Update();
				}

				pCurrentObj = pObjNext;
			}
		}
	}

	//deleted == true を delete & null代入
	for (int nCnt = 0; nCnt < MAX_PRIO; nCnt++)
	{
		CObject *pCurrentObj = m_pTop[nCnt];

		while (pCurrentObj != nullptr)
		{
			CObject *pObjNext = pCurrentObj->m_pNext;

			if (pCurrentObj->IsDeleted())
			{
				pCurrentObj->Release();
			}
			
			pCurrentObj = pObjNext;
		}
	}
}

//--------------------------------------------------
// すべての描画
//--------------------------------------------------
void CObject::DrawAll()
{
	for (int nCnt = 0; nCnt < MAX_PRIO; nCnt++)
	{
		if (m_pTop[nCnt] != nullptr)
		{
			CObject *pCurrentObj = m_pTop[nCnt];

			while (pCurrentObj != nullptr && !pCurrentObj->IsDeleted())
			{
				CObject *pObjNext = pCurrentObj->m_pNext;
				pCurrentObj->Draw();
				pCurrentObj = pObjNext;
			}
		}
	}
}

//--------------------------------------------------
// モード以外をリリース
//--------------------------------------------------
void CObject::ReleaseWithoutMode()
{
	for (int nCnt = 0; nCnt < MAX_PRIO; nCnt++)
	{
		CObject *pCurrentObj = m_pTop[nCnt];

		while (pCurrentObj != nullptr)
		{
			CObject *pObjNext = pCurrentObj->m_pNext;

			if (pCurrentObj->GetType() != TYPE_MODE)
			{
				if (!pCurrentObj->IsDeleted())
				{
					pCurrentObj->Uninit();
				}
			}
			pCurrentObj = pObjNext;
		}
	}
	//deleted == true を delete & null代入
	
}

//=============================================================================
// release関数
//=============================================================================
void CObject::NotRelease()
{
	m_bDeleted = false;
}

//--------------------------------------------------
// deleteをtrueにする
//--------------------------------------------------
void CObject::DeletedObj()
{
	m_bDeleted = true;
}

//--------------------------------------------------
// リリース
//--------------------------------------------------
void CObject::Release()
{
	CObject *pDeleteObj = this;

	if (pDeleteObj == nullptr)
	{// 消そうとしてるやつが使われているかどうか
		return;
	}
	else if (m_pPrev == nullptr && m_pNext == nullptr)
	{// オブジェクトが秘湯しかないとき
		m_pTop[m_nPriority] = nullptr;
		m_pCurrent[m_nPriority] = nullptr;
	}
	else if (m_pPrev == nullptr)
	{// Topが消えた時
		m_pTop[m_nPriority] = pDeleteObj->m_pNext;
		m_pTop[m_nPriority]->m_pPrev = nullptr;
	}
	else if (m_pNext == nullptr)
	{// Currentが消えた時
		m_pCurrent[m_nPriority] = pDeleteObj->m_pPrev;
		m_pCurrent[m_nPriority]->m_pNext = nullptr;
	}
	else
	{// 間のオブジェクトが消えた時
		pDeleteObj->m_pPrev->m_pNext = pDeleteObj->m_pNext;
		pDeleteObj->m_pNext->m_pPrev = pDeleteObj->m_pPrev;
	}

	delete pDeleteObj;
}

//--------------------------------------------------
// 球の当たり判定
//--------------------------------------------------
bool IsCollisionSphere(D3DXVECTOR3 targetPos, D3DXVECTOR3 pos, D3DXVECTOR3 targetSize, D3DXVECTOR3 size)
{
	bool bIsLanding = false;

	// 目的の距離
	float fTargetDistance = sqrtf((targetPos.x - pos.x) * (targetPos.x - pos.x) +
		(targetPos.y - pos.y) * (targetPos.y - pos.y) +
		(targetPos.z - pos.z) * (targetPos.z - pos.z));
	// 現在の距離
	float fCurrentDistance = sqrtf((targetSize.x + size.x) * (targetSize.x + size.x) +
		(targetSize.y + size.y) * (targetSize.y + size.y) +
		(targetSize.z + size.z) * (targetSize.z + size.z));

	if (fTargetDistance < fCurrentDistance)
	{
		bIsLanding = true;
	}

	return bIsLanding;
}

//--------------------------------------------------
// 矩形の当たり判定
//--------------------------------------------------
bool IsCollision(D3DXVECTOR3 targetPos, D3DXVECTOR3 pos, D3DXVECTOR3 targetSize, D3DXVECTOR3 size)
{
	bool bIsLanding = false;

	if (pos.x + size.x / 2.0f >= targetPos.x - targetSize.x / 2.0f
		&& pos.x - size.x / 2.0f <= targetPos.x + targetSize.x / 2.0f
		&& pos.y - size.y / 2.0f <= targetPos.y + targetSize.y / 2.0f
		&& pos.y + size.y / 2.0f >= targetPos.y - targetSize.y / 2.0f)
	{
		bIsLanding = true;
	}

	return bIsLanding;
}

//=============================================================================
// SelectModelObject関数
// Author : hamada ryuga
//=============================================================================
CObject* CObject::SelectModelObject(int IsNumber, OBJECT_TYPE Type)
{
	int SetNumber = 0;
	for (int nCnt = 0; nCnt < MAX_PRIO; nCnt++)
	{//プライオリティの数回す
		
		CObject *obj = m_pTop[nCnt];
		if (obj != nullptr)
		{
			while (obj)
			{
				if (obj->m_type == Type)
				{//特定のTypeを探す

					if (obj->m_pNext == nullptr)
					{
						if (SetNumber >= IsNumber)
						{//一定回数以上なら情報を返す

							return obj;
						}
						break;
					}
					else
					{
						if (SetNumber >= IsNumber)
						{
							//一定回数以上なら情報を返す
							return obj;
						}
						SetNumber++;
						CObject *objnext = obj->m_pNext;
						obj = objnext;
					}
				}
				else
				{
					if (obj->m_pNext == nullptr)
					{//次のオブジェクトがなかったら破棄
						break;
					}
					else
					{
						CObject *objnext = obj->m_pNext;
						obj = objnext;
					}
				}
			}
		}
	}

	//情報がなかったらNULLを返す
	return nullptr;
}
