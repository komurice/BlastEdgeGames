//=============================================================================
//
// 説明書
// Author : 浜田琉雅
//
//=============================================================================


#ifndef _STAGE_H_			// このマクロ定義がされてなかったら
#define _STAGE_H_			// 二重インクルード防止のマクロ定義

#include "renderer.h"
#include "object.h"
#include "player3D.h"
#include "player.h"
class CMesh;
class CPlayer3D;
class CPlayer;
class CBuilding;
class CStage : public CObject
{
public:
	static const int MAX_MODELDATA = 256;	// ファイルpassの最大数

	const std::string REL_PATH = "data/TEXTURE/";

	const std::string ABS_PATH = "data\\TEXTURE\\";

	enum MESHTYPE
	{
		MESH = 0,		// 通常
		MESHMOVE,		// 動くゆか
		FLOORINGMOVE,	//滑る床
		RUNMESH,		//乗ったら動く床
		MAXMESH			//あんただれや？
	};


	enum MODELTYPE
	{
		NORMAL = 0,		// 通常
		PENDULUM,		//鉄球
		WOOD,			//まるた
		BALL,			//回転鉄球
		WOOL,			//NULL
		CHANGE,			//NULL
		GOAL,			//NULL
		MAXMODEL				//あんただれや？
	};

	void SetObject();

	void Loadfile(const char * pFileName);

	void LoadfileMesh(const char * pFileName);

	CStage();
	~CStage() override;
	HRESULT Init()override;
	void Uninit() override;
	void Update() override;
	void Draw()override {} ;

	static CStage* Create();

	static CMesh* GetMesh(int IsNumber) { return m_mesh[IsNumber]; }

	
	CMesh* GetPlayerHitMesh(int IsNumber);

	CPlayer * GetPlayer() { return m_pPlayer; };
	
	static void SetCountdown(bool countdown) { m_bCountdown = countdown; }
	static bool GetCountdown() { return m_bCountdown; }

	void SetNumAllBuilding(int IsBuilding) { m_numAllBuilding = IsBuilding; };	// 総数の取得
	int GetNumAllBuilding() { return m_numAllBuilding; };	// 総数の取得
private:

	CPlayer *m_pPlayer;
	static CMesh* m_mesh[100];
	int m_numAllBuilding;
	static bool m_bCountdown;
};
#endif
