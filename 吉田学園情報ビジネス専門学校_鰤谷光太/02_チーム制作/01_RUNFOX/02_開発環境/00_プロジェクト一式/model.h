//==================================================
// model.h
// Author: Buriya Kota
//==================================================
#ifndef _MODEL_H_
#define _MODEL_H_

//**************************************************
// インクルード
//**************************************************
#include "object.h"
#include "texture.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// マクロ定義
//**************************************************
#define MAX_MODEL				(54)
#define MAX_TEXTURE				(54)

//**************************************************
// クラス
//**************************************************
class CModel
{
public:
	enum MODEL_TYPE
	{
		MODEL_TYPE_NONE = -1,
		MODEL_TYPE_PLAYER,
		MODEL_TYPE_MAX
	};

	struct MODEL
	{
		// メッシュ(頂点の集まり)情報へのポインタ
		LPD3DXMESH mesh;
		// マテリアル情報へのポインタ	←　1のXファイルに複数のマテリアルが入っている
		LPD3DXBUFFER buffMat;
		// マテリアル情報の数
		DWORD numMat;
		// テクスチャの列挙型
		LPDIRECT3DTEXTURE9 texture[MAX_TEXTURE];
	};

	CModel();
	~CModel();

	HRESULT Init(int nType);
	void Uninit();
	void Update();
	void Draw();

	static CModel *Create(int nType);

	// セッター
	void SetPos(const D3DXVECTOR3& pos) { m_pos = pos; }
	void SetScale(const D3DXVECTOR3& scale) { m_scale = scale; }
	void SetRot(const D3DXVECTOR3& rot) { m_rot = rot; }
	void SetParent(CModel *pModel) { m_pParent = pModel; }

	// ゲッター
	const D3DXVECTOR3& GetPos() const { return m_pos; }
	const D3DXVECTOR3& GetScale() const { return m_scale; }
	const D3DXVECTOR3& GetRot() const { return m_rot; }
	D3DXMATRIX GetMtxWorld() { return m_mtxWorld; }

	static void LoadModel(const char *pFile, int nNum);
	static void UnLoadModel();



private:
	void DrawShadow_();

private:
	// 頂点バッファへのポインタ
	LPDIRECT3DVERTEXBUFFER9 m_pVtxBuff;
	// ワールドマトリックス
	D3DXMATRIX m_mtxWorld;
	// 位置
	D3DXVECTOR3 m_pos;
	// 大きさ
	D3DXVECTOR3 m_scale;
	// 向き
	D3DXVECTOR3 m_rot;
	// 最小値
	D3DXVECTOR3 m_vtxMinModel;
	// 最大値
	D3DXVECTOR3 m_vtxMaxModel;
	// モデルの情報
	static MODEL m_model[MAX_MODEL];
	// 親の情報
	CModel *m_pParent;
	// タイプ
	int m_nType;

};

#endif	// _MODEL_H_