//==================================================
// objectX.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "objectX.h"
#include "renderer.h"
#include "camera.h"
#include "light.h"

//**************************************************
// 静的メンバ変数
//**************************************************
const float CObjectX::BILLBOARD_HEIGHT = 10.0f;
const float CObjectX::BILLBORAD_WIDTH = 10.0f;
CObjectXManager * CObjectXManager::ms_ObjectXManager;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CObjectX::CObjectX(int nPriority /* =3 */) : CObject(nPriority)
{
	// 位置
	m_posOrigin = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	// 向き
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	// 最小値
	m_vtxMinModel = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	// 最大値
	m_vtxMaxModel = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	// 大きさ
	m_size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CObjectX::~CObjectX()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CObjectX::Init()
{
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CObjectX::Uninit()
{
	if (m_modelXData != nullptr)
	{
		delete m_modelXData;
		m_modelXData = nullptr;
	}
	
	DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CObjectX::Update()
{
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CObjectX::Draw()
{
	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();
	// 計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans, mtxScale;
	// 現在のマテリアル保存用
	D3DMATERIAL9 matDef;
	// マテリアルデータへのポインタ
	D3DXMATERIAL *pMat;

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxWorld);

	// 向きを反映
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);

	// 位置を反映
	D3DXMatrixTranslation(&mtxTrans, m_posOrigin.x, m_posOrigin.y, m_posOrigin.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);

	// ワールドマトリックスの設定
	pDevice->SetTransform(D3DTS_WORLD, &m_mtxWorld);

	// 現在のマテリアル保持
	pDevice->GetMaterial(&matDef);

	// マテリアルデータへのポインタを取得
	pMat = (D3DXMATERIAL*)m_modelXData-> m_buffMat->GetBufferPointer();

	for (int i = 0; i < (int)m_modelXData-> m_numMat; i++)
	{
		pMat[i].MatD3D.Ambient = pMat[i].MatD3D.Diffuse;
		// マテリアルの設定
		pDevice->SetMaterial(&pMat[i].MatD3D);

		// テクスチャの設定
		pDevice->SetTexture(0, m_modelXData-> m_texture[i]);

		// モデルパーツの描画
		m_modelXData-> m_mesh->DrawSubset(i);
	}

	// 保存していたマテリアルを戻す
	pDevice->SetMaterial(&matDef);

	// テクスチャの設定
	pDevice->SetTexture(0, NULL);
}

//--------------------------------------------------
// モデルのセット
//--------------------------------------------------
void CObjectX::SetModel(const char *filename)
{
	m_modelXData = new CModelX;

	// 頂点座標の最小値
	m_vtxMinModel = D3DXVECTOR3(100.0f, 100.0f, 100.0f);
	// 頂点座標の最大値
	m_vtxMaxModel = D3DXVECTOR3(-100.0f, -100.0f, -100.0f);

	CObjectXManager *Manager = CObjectXManager::GetManager();

	// モデルデータの読み込み
	CModelX*Data = Manager->LoadXfile(filename);

	// 保存してたデータのコピー
	m_modelXData->m_buffMat = Data->m_buffMat;
	m_modelXData->m_numMat = Data->m_numMat;
	m_modelXData->m_mesh = Data->m_mesh;
	m_vtxMaxModel = Data->GetVtxMax();
	m_vtxMinModel = Data->GetVtxMin();

	for (int i = 0; i < (int)Data->m_numMat; i++)
	{
		m_modelXData->m_texture[i] = Data->m_texture[i];
	}

}

//--------------------------------------------------
// 現在の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CObjectX::RotNormalization(D3DXVECTOR3 rot)
{
	m_rot = rot;

	// 現在の角度の正規化
	if (m_rot.y > D3DX_PI)
	{
		m_rot.y -= D3DX_PI * 2.0f;
	}
	else if (m_rot.y < -D3DX_PI)
	{
		m_rot.y += D3DX_PI * 2.0f;
	}

	return m_rot;
}

//--------------------------------------------------
// 目的の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CObjectX::RotDestNormalization(D3DXVECTOR3 rot, D3DXVECTOR3 rotDest)
{
	m_rot = rot;
	m_rotDest = rotDest;

	// 目的の角度の正規化
	if (m_rotDest.y - m_rot.y > D3DX_PI)
	{
		m_rotDest.y -= D3DX_PI * 2.0f;
	}
	else if (m_rotDest.y - m_rot.y < -D3DX_PI)
	{
		m_rotDest.y += D3DX_PI * 2.0f;
	}

	return m_rotDest;
}


//=============================================================================
// マネージャーのコンストラクタ
// Author :浜田琉雅
// 概要 : 数値の初期化
//=============================================================================
CObjectXManager::CObjectXManager()
{
	for (int i = 0; i < MODEL_MAX; i++)
	{
		m_modelXList[i] = nullptr;
	}
}

//=============================================================================
//  マネージャーのデス
// Author : 浜田琉雅
// 概要 : 
//=============================================================================
CObjectXManager::~CObjectXManager()
{

}

//=============================================================================
// カウントのリセット
// Author : 浜田琉雅
// 概要 : カウントのリセット
//=============================================================================
CObjectXManager * CObjectXManager::GetManager()
{
	if (ms_ObjectXManager == nullptr)
	{
		ms_ObjectXManager = new CObjectXManager;
	}
	return ms_ObjectXManager;
}

//=============================================================================
// Xfileのよみこみ
// Author : 浜田琉雅
// 概要 : Xfileのよみこみ
//=============================================================================
CModelX *CObjectXManager::LoadXfile(const char * pXFileName)
{
	for (int i = 0; i < MODEL_MAX; i++)
	{
		if (m_modelXList[i] == nullptr)
		{
			continue;
		}

		if (strcmp(&m_modelXList[i]->m_pXFileName[0], &pXFileName[0]) == 0)
		{// 同じデータがあったらもともとあるデータを返す
			m_modelXList[i]->m_nType = i;
			return m_modelXList[i];
		}
	}

	for (int i = 0; i < MODEL_MAX; i++)
	{
		if (m_modelXList[i] == nullptr)
		{
			m_modelXList[i] = new CModelX;

			strcpy(m_modelXList[i]->m_pXFileName, pXFileName);

			// Xファイルの読み込み
			D3DXLoadMeshFromX(pXFileName,
				D3DXMESH_SYSTEMMEM,
				CManager::GetRenderer()->GetDevice(),
				NULL,
				&m_modelXList[i]->m_buffMat,
				NULL,
				&m_modelXList[i]->m_numMat,
				&m_modelXList[i]->m_mesh);

			m_modelXList[i]->m_nType = i;

			int nNumVtx;		// 頂点数
			DWORD pSizeFVF;		// 頂点フォーマットのサイズ
			BYTE *pVtxBuff;		// 頂点バッファのポインタ

			// 頂点数の取得
			nNumVtx = m_modelXList[i]->m_mesh->GetNumVertices();

			// 頂点フォーマットのサイズを取得
			pSizeFVF = D3DXGetFVFVertexSize(m_modelXList[i]->m_mesh->GetFVF());

			// 頂点バッファのロック
			m_modelXList[i]->m_mesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&pVtxBuff);

			for (int nCntVtx = 0; nCntVtx < nNumVtx; nCntVtx++)
			{
				// 頂点座標の代入
				D3DXVECTOR3 vtx = *(D3DXVECTOR3*)pVtxBuff;

				// 比較(最小値を求める)x
				if (vtx.x < m_modelXList[i]->m_vtxMinModel.x)
				{
					m_modelXList[i]->m_vtxMinModel.x = vtx.x;
				}
				// 比較(最小値を求める)y
				if (vtx.y < m_modelXList[i]->m_vtxMinModel.y)
				{
					m_modelXList[i]->m_vtxMinModel.y = vtx.y;
				}
				// 比較(最小値を求める)z
				if (vtx.z < m_modelXList[i]->m_vtxMinModel.z)
				{
					m_modelXList[i]->m_vtxMinModel.z = vtx.z;
				}

				// 比較(最大値を求める)x
				if (vtx.x > m_modelXList[i]->m_vtxMaxModel.x)
				{
					m_modelXList[i]->m_vtxMaxModel.x = vtx.x;
				}
				// 比較(最大値を求める)y
				if (vtx.y > m_modelXList[i]->m_vtxMaxModel.y)
				{
					m_modelXList[i]->m_vtxMaxModel.y = vtx.y;
				}
				// 比較(最大値を求める)z
				if (vtx.z > m_modelXList[i]->m_vtxMaxModel.z)
				{
					m_modelXList[i]->m_vtxMaxModel.z = vtx.z;
				}

				// 頂点フォーマットのサイズ分ポインタを進める
				pVtxBuff += pSizeFVF;
			}
			LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

			// バッファの先頭ポインタをD3DXMATERIALにキャストして取得
			D3DXMATERIAL *pMat = (D3DXMATERIAL*)m_modelXList[i]->m_buffMat->GetBufferPointer();
	
			// 各メッシュのマテリアル情報を取得する
			for (int j = 0; j < (int)m_modelXList[i]->m_numMat; j++)
			{
				m_modelXList[i]->m_texture[j] = NULL;

				if (pMat[j].pTextureFilename != NULL)
				{// マテリアルで設定されているテクスチャ読み込み
					D3DXCreateTextureFromFileA(pDevice,
						pMat[j].pTextureFilename,
						&m_modelXList[i]->m_texture[j]);
				}
			}

			// 頂点バッファのアンロック
			m_modelXList[i]->m_mesh->UnlockVertexBuffer();

			return m_modelXList[i];
		}
	}

	assert(false);
	return nullptr;
}

//=============================================================================
// モデルの破棄
// Author : 浜田琉雅
// 概要 : モデルの破棄
//=============================================================================
void CObjectXManager::ReleaseAll()
{
	CObjectXManager *Manager = CObjectXManager::GetManager();
	if (Manager != nullptr)
	{
		for (int i = 0; i < MODEL_MAX; i++)
		{
			if (Manager->m_modelXList[i] != nullptr)
			{
				Manager->m_modelXList[i]->Release();
				delete Manager->m_modelXList[i];
				Manager->m_modelXList[i] = nullptr;
			}
		}
		if (ms_ObjectXManager != nullptr)
		{
			delete ms_ObjectXManager;
			ms_ObjectXManager = nullptr;
		}
	}
}
