//==================================================
// ball.cpp
// Author: tutida ryousei
//==================================================
#include"ball.h"

#include "debug_proc.h"
#include "stage.h"
#include "player.h"
#include "game.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CBall::CBall()
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CBall::~CBall()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CBall::Init()
{
	CBuilding::Init();


	// モデルの設定
	const char* Model = "data/MODEL/ball.x";
	SetModel(Model);

	// 回転の方向
	if (m_nRotType == 0)
	{
		m_Pos = { m_CenterPos.x - m_fRadius ,m_CenterPos.y,m_CenterPos.z };
	}
	else
	{
		m_Pos = { m_CenterPos.x + m_fRadius ,m_CenterPos.y,m_CenterPos.z };
	}

	return	S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CBall::Uninit()
{
	CBuilding::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CBall::Update()
{
	CBuilding::Update();

	// 移動
	Move();

	if (m_Pos.y < 300.0f)
	{
		m_Pos.y = 300.0f;
	}

	SetPos(m_Pos);
	SetRot(m_Rot);
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CBall::Draw()
{
	CBuilding::Draw();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CBall *CBall::Create(D3DXVECTOR3 centerpos, float radius, float rotspeed, int rottype)
{
	CBall *pBall = nullptr;

	pBall = new CBall;

	if (pBall != nullptr)
	{
		pBall->SetCenterPos(centerpos);
		pBall->SetRadius(radius);
		pBall->SetRotSpeed(rotspeed);
		pBall->SetRotType(rottype);
		pBall->Init();
	}

	return pBall;
}

//--------------------------------------------------
// 移動
//--------------------------------------------------
void CBall::Move()
{
	// 移動速度の算出
	m_fMoveSpeed = (m_fRadius / 10);
	float r = (0.1f / m_fRotSpeed);
	m_fMoveSpeed /= r;

	// 移動
	m_Pos.x += sinf(m_Rot.y) * m_fMoveSpeed;
	m_Pos.z += cosf(m_Rot.y) * m_fMoveSpeed;

	// 回転
	if (m_nRotType == 0)
	{// 右回り
		m_Rot.y += m_fRotSpeed;
	}
	else
	{// 左回り
		m_Rot.y -= m_fRotSpeed;
	}

	m_Rot.x += m_fRotSpeed * 2;
}

//------------------------------------
// 動き系統
//------------------------------------
void CBall::Hit()
{
	CGame::GetStage()->GetPlayer()->breakHit();
}
