//============================
//
// リザルト画面
// Author:hamada ryuuga
//
//============================

//------------------------
// インクルード
//------------------------
#include "result.h"
#include "input.h"
#include "manager.h"
#include "input_keyboard.h"
#include "input_joypad.h"
#include "object2D.h"
#include "fade.h"
#include "sound.h"
#include "text.h"
#include "playfab.h"

int CResult::m_Score;
std::string CResult::m_Name[5];


//========================
// コンストラクター
//========================
CResult::CResult(int nPriority /* =1 */) : CObject(nPriority)
{
}
//========================
// デストラクト
//========================
CResult::~CResult()
{
}

//========================
// リザルトの初期化処理
//========================
HRESULT CResult::Init(void)
{
	m_finished = false;
	m_Pop = false;
	m_timer = 0;
	//背景の配置
	SetBG();

	//ロゴの配置
	SetResultLogo();

	CResult::GetScore();

	CManager::GetSound()->Play(CSound::SOUND_BGM_RESULT);

	return S_OK;
}

//========================
// リザルトの終了処理
//========================
void CResult::Uninit(void)
{
	CManager::GetSound()->Stop(CSound::SOUND_BGM_RESULT);

	DeletedObj();
}

//========================
// リザルトの更新処理
//========================
void CResult::Update(void)
{
	
	CSound* pSound = CManager::GetSound();		//サウンドの情報取得
	CInputKeyboard *pInputKeyoard = CManager::GetInputKeyboard();
	CInputJoyPad *pInputJoyPad = CManager::GetInputJoyPad();

	if (m_finished && ! m_Pop)
	{
		for (int i = 0; i < 3; i++)
		{
			CText::Create(CText::GON, 1000000000, 1, m_Name[i].c_str(), D3DXVECTOR3(1300.0f, 100.0f + (200.0f* i), 0), D3DXVECTOR3(900.0f, 300.0f, 0), D3DXVECTOR3(30.0f, 40.0f, 0));
		}
		m_Pop = true;
	}
	if (!m_finished)
	{
		m_timer++;
		if (m_timer >= 3000)
		{
			CText::Create(CText::GON, 1000000000, 1, "ランキングしゅとくしっぱい！", D3DXVECTOR3(900.0f, 200.0f + (200.0f), 0), D3DXVECTOR3(900.0f, 300.0f, 0), D3DXVECTOR3(30.0f, 30.0f, 0));
			m_Pop = true;
		}
	}
	if ((pInputKeyoard->GetTrigger(DIK_RETURN) || pInputJoyPad->GetJoypadTrigger(pInputJoyPad->JOYKEY_A, 0) || pInputJoyPad->GetJoypadTrigger(pInputJoyPad->JOYKEY_B, 0)) && m_Pop)
	{
		pSound->Play(CSound::SOUND_SE_CANCEL);
		CManager::GetFade()->NextMode(CManager::MODE_TITLE);
	}
}

//========================
// リザルトの描画処理
//========================
void CResult::Draw(void)
{
}

//==================
//クリエイト
//==================
CResult*CResult::Create()
{
	CResult *pResult = nullptr;			//ポインタにヌルを代入

	pResult = new CResult;				//動的確保

	if (pResult != nullptr)
	{//ポインタに値が入っていたら
		pResult->Init();				//初期化
		pResult->SetType(TYPE_MODE);	//モード設定
	}

	return pResult;
}

//==================
//背景の配置
//==================
void CResult::SetBG()
{
	//ポリゴンの生成
	CObject2D *pObject2D = CObject2D::Create({ 1280 / 2,720 / 2,0.0f }, { 1280.0f,720.0f,0.0f }, 1);

	//テクスチャの設定
	pObject2D->SetTexture(CTexture::TEXTURE_RESULT);
}

//==================
//ロゴの配置
//==================
void CResult::SetResultLogo()
{
	//ポリゴンの生成
	CObject2D *pObject2D = CObject2D::Create(D3DXVECTOR3(300.0f, 150.0f, 0.0f), D3DXVECTOR3(600.0f, 400.0f, 0.0f), 1);

	//テクスチャの設定
	pObject2D->SetTexture(CTexture::TEXTURE::TEXTURE_RESULT_TITLE);
}

//==================
//ランキングの配置
//==================
void CResult::GetScore()
{
	CPlayfab::GetScore([this](const ClientModels::GetLeaderboardResult& resul) {

		for (auto item : resul.Leaderboard)
		{
			if (item.Position <= 2)
			{
				switch (item.Position)
				{
				case 0:
					m_Name[item.Position] = "１位	";
					break;
				case 1:
					m_Name[item.Position] = "２位	";
					break;
				case 2:
					m_Name[item.Position] = "３位	";
					break;
				case 3:
					break;
				default:
					break;
				}
				m_Name[item.Position] += item.DisplayName;//なまえをキャラに変換
				m_Name[item.Position] += "\nscore	";//なまえをキャラに変換
														  // 表示
				m_Name[item.Position] += std::to_string(item.StatValue);

			
			}
		}
		this->m_finished = true;
	});
}

