//==================================================
// player3D.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "manager.h"

#include "fade_model.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CFadeModel::CFadeModel(int nPriority) : CObjectX(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CFadeModel::~CFadeModel()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CFadeModel::Init()
{
	SetModel("data/MODEL/Fox/fox.x");
	SetPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CFadeModel::Update()
{
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CFadeModel::Draw()
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

	// Zテストを失敗させる
	//pDevice->SetRenderState(D3DRS_ZFUNC, D3DCMP_NEVER);
	// ステンシルバッファ -> 有効
	pDevice->SetRenderState(D3DRS_STENCILENABLE, TRUE);

	// ステンシルバッファと比較する参照値設定 -> ref
	pDevice->SetRenderState(D3DRS_STENCILREF, 0x10);

	// ステンシルバッファの値に対してのマスク設定 -> 0xff(全て真)
	pDevice->SetRenderState(D3DRS_STENCILMASK, 0xff);

	// ステンシルテストの比較方法 ->
	// （参照値 >= ステンシルバッファの参照値）なら合格
	pDevice->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_GREATEREQUAL);

	// ステンシルテストの結果に対しての反映設定
	pDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_KEEP);		// Zとステンシル成功
	pDevice->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);			// Zとステンシル失敗
	pDevice->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_REPLACE);			// Zのみ失敗

	CObjectX::Draw();

	// ステンシルバッファ -> 無効
	pDevice->SetRenderState(D3DRS_STENCILENABLE, FALSE);
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CFadeModel *CFadeModel::Create()
{
	CFadeModel *pFadeModel;
	pFadeModel = new CFadeModel;

	if (pFadeModel != nullptr)
	{
		pFadeModel->Init();
	}
	else
	{
		assert(false);
	}

	return pFadeModel;
}
