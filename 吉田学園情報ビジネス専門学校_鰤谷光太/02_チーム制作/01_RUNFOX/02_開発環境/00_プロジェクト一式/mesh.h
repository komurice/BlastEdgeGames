//============================
//
// メッシュ設定ヘッター
// Author:hamada ryuuga
//
//============================
#ifndef _MESH_H_
#define _MESH_H_

#include "main.h"
#include "object.h"
#include "object3D.h"


#define	EMESHX	(1)
#define	EMESHY	(5)
#define MAX_SIZEMESH (100.0f)
#define MAX_EMESH (10)
#define MOUNTAIN (50.0f)
#define MAXMOVE (10)

#define EMESHMAX (12800)


class CMesh : public CObject
{
public:
	CMesh(int nPriority = PRIORITY_OBJECT);
	~CMesh() override;

	HRESULT Init()override;//初期化
	void Uninit()override;//破棄
	void Update()override;//更新
	void Draw()override;//描画



	static CMesh* Create();
	void SetPos(const D3DXVECTOR3 &pos);
	const D3DXVECTOR3 * CMesh::GetPos() const;
	bool CollisionMesh(D3DXVECTOR3 *pPos, float *move, bool jump);

	void Loadfile(const char * pFileName);

	D3DXVECTOR3 GetMeshSize() { return D3DXVECTOR3(m_X *m_MeshSize.x, 0.0f, m_Z *m_MeshSize.z); }
	D3DXVECTOR3 GetOneMeshSize() { return m_MeshSize; }

	D3DXVECTOR3 * GetPos() { return &m_posOrigin; }

	void SetMesh(int Size);
	void SetNumber(int IsNumber) { m_Number = IsNumber; }
	void SetType(int IsType) { m_Type = IsType; }
	void SetTexture(const char * pFileName);

	bool MoveMesh(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pMove);

	float GetMove() { return m_move; }
	void SetMove(float ismove) { m_move = ismove; }

	int GetNumber() { return m_Number; }
	int GetMeshType() { return m_Type; }
	virtual void OnHit() {}

private:
	void SetVtxMesh(VERTEX_3D* pVtx, WORD* pIdx, int nCnt, bool isUp);
	void SetVtxMeshSize(int Size);
	void SetVtxMeshLight();

	LPDIRECT3DVERTEXBUFFER9 m_pVtxBuff;	    // 頂点バッファーへのポインタ
	LPDIRECT3DTEXTURE9 m_pTextureEmesh;        //テクスチャのポインタ
	LPDIRECT3DINDEXBUFFER9 m_pIdxBuff;         //インデックスバッファ

	D3DXVECTOR3 m_pos;	// 頂点座標
	D3DXVECTOR3 m_posOrigin;	// 頂点座標
	D3DXVECTOR3 m_rot;	// 回転座標
	D3DXVECTOR3 m_MeshSize;
	D3DXMATRIX m_mtxWorld;// ワールドマトリックス
	int m_xsiz;//面数
	int m_zsiz;//面数
	int m_X;//辺の頂点数
	int m_Z;//辺の頂点数
	int m_nVtx;//頂点数
	int m_Index; //インデックス
	int m_por;
	int m_Number;
	int m_Type;
	float m_move;
	std::string  m_pFileName;

	
	

};
#endif

