//==================================================
// game_end.h
// Author: tutida ryousei
//==================================================
#ifndef _GAME_END_H_
#define	_GAME_END_H_

#include"object2D.h"

class CGameEnd : public CObject2D
{
public:
	explicit CGameEnd(int nPriority = PRIORITY_UI);
	~CGameEnd();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	static CGameEnd *Create(D3DXVECTOR3 pos, D3DXVECTOR3 size, int displaytime, float alphaspeed, CTexture::TEXTURE texture);
	
	// セッター
	void SetDisplayTime(int displaytime) { m_nDisplayTime = displaytime; }
	void SetAlphaSpeed(float alphaspeed) { m_fAlphaSpeed = alphaspeed; }
	void SetTex(CTexture::TEXTURE texture) { m_Texture = texture; }

	// ゲッター
	bool GetEnd() { return m_bEnd; }

private:
	D3DXVECTOR3 m_Pos;				// 位置
	D3DXVECTOR3 m_Size;				// サイズ
	int m_nDisplayTime;				// 表示時間
	float m_fAlpha;					// アルファ値
	float m_fAlphaSpeed;			// アルファ値の減少スピード
	bool m_bEnd;					// 終わったか
	CObject2D *m_pObject2D;			// オブジェクト2Dの情報
	CTexture::TEXTURE m_Texture;	// テクスチャの情報
};

#endif // !_GAME_END_H_
