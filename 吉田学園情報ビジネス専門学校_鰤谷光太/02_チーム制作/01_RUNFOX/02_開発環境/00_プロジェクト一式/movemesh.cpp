//============================
//
// 動くmesh設定
// Author:hamada ryuuga
//
//============================

#include "movemesh.h"
#include "utility.h"
#include "stage.h"
#include "game.h"

//------------------------------------
// コンストラクタ
//------------------------------------
CMoveMesh::CMoveMesh()
{
}

//------------------------------------
// デストラクタ
//------------------------------------
CMoveMesh::~CMoveMesh()
{
}

//------------------------------------
// 初期化
//------------------------------------
HRESULT CMoveMesh::Init()
{
	CMesh::Init();

	m_Testrot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	return S_OK;
}

//------------------------------------
// 終了
//------------------------------------
void CMoveMesh::Uninit()
{
	CMesh::Uninit();
}

//------------------------------------
// 更新
//------------------------------------
void CMoveMesh::Update()
{
	CMesh::Update();
	//動き
	CMoveMesh::move();
}

//------------------------------------
// 描画
//------------------------------------
void CMoveMesh::Draw()
{
	CMesh::Draw();
}

//------------------------------------
// create
//------------------------------------
CMoveMesh *CMoveMesh::Create()
{
	CMoveMesh * pObject = new CMoveMesh;

	if (pObject != nullptr)
	{
		pObject->Init();
	}
	return pObject;
}


//------------------------------------
// 動き系統
//------------------------------------
void CMoveMesh::move()
{

}

//------------------------------------
// Playerが当たった時の判定
//------------------------------------
void CMoveMesh::OnHit()
{

	m_movemesh = CGame::GetStage()->GetPlayer()->GetMove();

	m_movemesh.x -= GetMove();
	//ゆかの動き設定
	CGame::GetStage()->GetPlayer()->SetMove(m_movemesh.x);

	CGame::GetStage()->GetPlayer()->SetFriction(-0.3f);
}