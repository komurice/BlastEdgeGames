//**************************************************
// 
// texture.cpp
// Author  : katsuki mizuki
// 
//**************************************************

//==================================================
// インクルード
//==================================================
#include "manager.h"
#include "renderer.h"
#include "texture.h"

#include <assert.h>

//==================================================
// 定義
//==================================================
const char* CTexture::s_FileName[] =
{// テクスチャのパス
	"data/TEXTURE/hasegawa_nagi.png",		// プレイヤー
	"data/TEXTURE/thumbnail.png",			// 地面
	"data/TEXTURE/bg.jpg",					// 空
	"data/TEXTURE/shadow000.jpg",			// 影
	"data/TEXTURE/number.png",				// 数字
	"data/NEWUI/migiue.png",				// 時計
	"data/TEXTURE/start.png",				// スタート
	"data/TEXTURE/pause0.png",				// ポーズ
	"data/TEXTURE/pause1.png",				// ポーズ１
	"data/TEXTURE/title.png",				// タイトル
	"data/TEXTURE/foxrun_F.png",			// F
	"data/TEXTURE/foxrun_O.png",			// O
	"data/TEXTURE/foxrun_X.png",			// X
	"data/TEXTURE/foxrun_R.png",			// R
	"data/TEXTURE/foxrun_U.png",			// U
	"data/TEXTURE/foxrun_N.png",			// N
	"data/TEXTURE/Result.png",				// リザルト
	"data/TEXTURE/goal.png",				// ゴール
	"data/TEXTURE/timeup.png",				// タイムアップ
	"data/TEXTURE/fox_run.png",				// タイトルロゴ
	"data/TEXTURE/LogoFOX.png",				// タイトルゴン
	"data/TEXTURE/SelectLogo1.png",			// タイトルゲームロゴ
	"data/TEXTURE/SelectLogo2.png",			// タイトルチュートリアルロゴ
	"data/TEXTURE/SelectLogo3.png",			// タイトルランキングロゴ
	"data/TEXTURE/SelectLogo4.png",			// タイトル終了ロゴ
	"data/TEXTURE/Frame.png",				// タイトル選択フレーム
	"data/TEXTURE/ResultLogo.png",			// リザルトロゴ
	"data/TEXTURE/TemporaryRanking.png",	// リザルトロゴ
	"data/TEXTURE/TutorialLogo.png",		// チュートリアルロゴ
	"data/TEXTURE/TaitoruNiModoru.png",		// チュートリアルロゴ
	"data/TEXTURE/setumei.png",				// チュートリアル説明
	"data/TEXTURE/tutorialland.png",		// チュートリアル開始ぃぃぃぃ
	"data/TEXTURE/LandD.png",				// チュートリアル本編
	"data/TEXTURE/LandDD.png",				// チュートリアル本編１
	"data/TEXTURE/LandDs.png",				// チュートリアル本編２
	"data/TEXTURE/NameSet_BG.png",			// ネームセット背景
	"data/TEXTURE/checkpointUI.png",		// チェックポイント
	"data/TEXTURE/alphabet_ucase.png",		// チュートリアル本編５
};

static_assert(sizeof(CTexture::s_FileName) / sizeof(CTexture::s_FileName[0]) == CTexture::TEXTURE_MAX, "aho");

//--------------------------------------------------
// デフォルトコンストラクタ
//--------------------------------------------------
CTexture::CTexture() :
	s_pTexture()
{
	memset(s_pTexture, 0, sizeof(s_pTexture));
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CTexture::~CTexture()
{
}

//--------------------------------------------------
// 全ての読み込み
//--------------------------------------------------
void CTexture::LoadAll()
{
	// デバイスへのポインタの取得
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

	for (int i = 0; i < TEXTURE_MAX; ++i)
	{
		if (s_pTexture[i] != nullptr)
		{// テクスチャの読み込みがされている
			continue;
		}

		// テクスチャの読み込み
		D3DXCreateTextureFromFile(pDevice,
			s_FileName[i],
			&s_pTexture[i]);
	}
}

//--------------------------------------------------
// 読み込み
//--------------------------------------------------
void CTexture::Load(TEXTURE inTexture)
{
	assert(inTexture >= 0 && inTexture < TEXTURE_MAX);

	if (s_pTexture[inTexture] != nullptr)
	{// テクスチャの読み込みがされている
		return;
	}

	// デバイスへのポインタの取得
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

	// テクスチャの読み込み
	D3DXCreateTextureFromFile(pDevice,
		s_FileName[inTexture],
		&s_pTexture[inTexture]);
}

//--------------------------------------------------
// 全ての解放
//--------------------------------------------------
void CTexture::ReleaseAll(void)
{
	for (int i = 0; i < TEXTURE_MAX; ++i)
	{
		if (s_pTexture[i] != nullptr)
		{// テクスチャの解放
			s_pTexture[i]->Release();
			s_pTexture[i] = nullptr;
		}
	}
}

//--------------------------------------------------
// 解放
//--------------------------------------------------
void CTexture::Release(TEXTURE inTexture)
{
	assert(inTexture >= 0 && inTexture < TEXTURE_MAX);

	if (s_pTexture[inTexture] != nullptr)
	{// テクスチャの解放
		s_pTexture[inTexture]->Release();
		s_pTexture[inTexture] = nullptr;
	}
}

//--------------------------------------------------
// 取得
//--------------------------------------------------
LPDIRECT3DTEXTURE9 CTexture::GetTexture(TEXTURE inTexture)
{
	if (inTexture == TEXTURE_NONE)
	{// テクスチャを使用しない
		return nullptr;
	}

	assert(inTexture >= 0 && inTexture < TEXTURE_MAX);

	// 読み込み
	Load(inTexture);

	return s_pTexture[inTexture];
}
