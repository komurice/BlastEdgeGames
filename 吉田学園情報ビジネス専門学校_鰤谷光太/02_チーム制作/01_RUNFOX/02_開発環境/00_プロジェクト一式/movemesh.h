//=============================================================================
//
// 説明書
// Author : 浜田琉雅
//
//=============================================================================


#ifndef _MOVEMESH_H_			// このマクロ定義がされてなかったら
#define _MOVEMESH_H_			// 二重インクルード防止のマクロ定義

#include "renderer.h"
#include "mesh.h"

class CMoveMesh : public CMesh
{
public:

	CMoveMesh();
	~CMoveMesh() override;
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	static CMoveMesh* Create();
private:
	D3DXVECTOR3 m_Testrot;
	D3DXVECTOR3 m_movemesh;
	void move();
	void OnHit() override;
};
#endif

