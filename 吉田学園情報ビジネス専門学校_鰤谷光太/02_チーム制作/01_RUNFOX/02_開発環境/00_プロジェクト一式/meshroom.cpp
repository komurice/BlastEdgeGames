//============================
//
// 動くmesh設定
// Author:hamada ryuuga
//
//============================

#include "meshroom.h"
#include "utility.h"
#include "stage.h"
#include "manager.h"
#include "game.h"
//------------------------------------
// コンストラクタ
//------------------------------------
CMeshRoom::CMeshRoom()
{
}

//------------------------------------
// デストラクタ
//------------------------------------
CMeshRoom::~CMeshRoom()
{
}

//------------------------------------
// 初期化
//------------------------------------
HRESULT CMeshRoom::Init()
{
	CMesh::Init();

	
	return S_OK;
}

//------------------------------------
// 終了
//------------------------------------
void CMeshRoom::Uninit()
{
	CMesh::Uninit();
}

//------------------------------------
// 更新
//------------------------------------
void CMeshRoom::Update()
{
	CMesh::Update();
	//動き
	CMeshRoom::move();
}

//------------------------------------
// 描画
//------------------------------------
void CMeshRoom::Draw()
{
	CMesh::Draw();
}

//------------------------------------
// create
//------------------------------------
CMeshRoom *CMeshRoom::Create()
{
	CMeshRoom * pObject = new CMeshRoom;

	if (pObject != nullptr)
	{
		pObject->Init();
	}
	return pObject;
}


//------------------------------------
// 動き系統
//------------------------------------
void CMeshRoom::move()
{
	
}

//------------------------------------
// Playerが当たった時の判定
//------------------------------------
void CMeshRoom::OnHit()
{
	//移動速度制限設定
	CGame::GetStage()->GetPlayer()->SetMaxMove(10.0f);
	CGame::GetStage()->GetPlayer()->SetFriction(-0.48f);
}