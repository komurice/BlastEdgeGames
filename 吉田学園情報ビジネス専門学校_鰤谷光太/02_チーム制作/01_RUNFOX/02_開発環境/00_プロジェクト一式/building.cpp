//============================
//
// 建物
// Author:hamada ryuuga
//
//============================

#include "building.h"
#include "hamada.h"
#include "manager.h"
#include "utility.h"

#include "stage.h"
#include "game.h"
#include "sound.h"
#include "fade.h"
#include "nameset.h"
#include "playfab.h"
#include "result.h"
#include "game.h"
#include "timer.h"
#include "game_end.h"
int CBuilding::m_NowNumber;

//------------------------------------
// コンストラクタ
//------------------------------------
CBuilding::CBuilding(int nPriority) : CObjectX(nPriority)
{
	m_pObj2D = nullptr;					//オブジェクト2Dのメンバ変数の初期化
	m_nCheckCounter = 0;				//確認変数のクリア
}

//------------------------------------
// デストラクタ
//------------------------------------
CBuilding::~CBuilding()
{
}

//------------------------------------
// 初期化
//------------------------------------
HRESULT CBuilding::Init()
{
	m_nCheckCounter = -1;		//確認変数の初期化
	m_Gold = false;
	CObjectX::Init();

	SetModelNumber(m_NowNumber);

	m_NowNumber++;
	//Type設定
	SetType(TYPE_BUILDING);

	return E_NOTIMPL;
}

//------------------------------------
// 終了
//------------------------------------
void CBuilding::Uninit()
{
	CObjectX::Uninit();
}

//------------------------------------
// 更新
//------------------------------------
void CBuilding::Update()
{	
	//動き
	CBuilding::move();

	CObjectX::Update();

}

//------------------------------------
// 描画
//------------------------------------
void CBuilding::Draw()
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();
	//アルファブレンディングを加算合成に設定
	//pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	//pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	//pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);

	//Ｚ軸で回転しますちなみにm_rotつかうとグルグル回ります
	
	D3DXMATRIX mtxWorld = GetMtxWorld();

	mtxWorld = *hmd::giftmtx(&mtxWorld, GetPos(), GetRot());

	SetWorldMtx(mtxWorld);

	CObjectX::Draw();

	//αブレンディングを元に戻す
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	// ステンシルバッファ -> 有効
	pDevice->SetRenderState(D3DRS_STENCILENABLE, TRUE);

	// ステンシルバッファと比較する参照値設定 -> ref
	pDevice->SetRenderState(D3DRS_STENCILREF, 0x01);

	// ステンシルバッファの値に対してのマスク設定 -> 0xff(全て真)
	pDevice->SetRenderState(D3DRS_STENCILMASK, 0xff);

	// ステンシルテストの比較方法 ->
	// （参照値 >= ステンシルバッファの参照値）なら合格
	pDevice->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_EQUAL);

	// ステンシルテストの結果に対しての反映設定
	pDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_INCR);			// Zとステンシル成功
	pDevice->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);			// Zとステンシル失敗
	pDevice->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);			// Zのみ失敗

	CObjectX::Draw();

	// ステンシルバッファ -> 無効
	pDevice->SetRenderState(D3DRS_STENCILENABLE, FALSE);
}

//------------------------------------
// create
//------------------------------------
CBuilding *CBuilding::Create(const char * pFileName,D3DXVECTOR3 *pPos)
{
	CBuilding * pObject = nullptr;
	pObject = new CBuilding();

	if (pObject != nullptr)
	{
		pObject->Init();
		pObject->SetModel(pFileName);
		//pObject->SetUp(BUILDING);
		pObject->SetPos(*pPos);
		pObject->SetMove(D3DXVECTOR3(0.0f,0.0f,0.0f));
		pObject->SetFileName(pFileName);
	}
	return pObject;
}

//--------------------------------------------------
// 当たり判定　線分
//--------------------------------------------------
bool CBuilding::CollisionModel(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pPosOld, D3DXVECTOR3 *pSize)
{
	D3DXMATRIX mtxWorld = CObjectX::GetMtxWorld();

	bool bIsLanding = false;

	D3DXVECTOR3 min = CObjectX::GetVtxMin();
	D3DXVECTOR3 max = CObjectX::GetVtxMax();

	// 座標を入れる箱
	D3DXVECTOR3 localPos[4];
	D3DXVECTOR3 worldPos[4];

	// ローカルの座標
	localPos[0] = D3DXVECTOR3(min.x, 0.0f, max.z);
	localPos[1] = D3DXVECTOR3(max.x, 0.0f, max.z);
	localPos[2] = D3DXVECTOR3(max.x, 0.0f, min.z);
	localPos[3] = D3DXVECTOR3(min.x, 0.0f, min.z);

	for (int nCnt = 0; nCnt < 4; nCnt++)
	{// ローカルからワールドに変換
		D3DXVECTOR3 vec = localPos[nCnt];
		D3DXVec3Normalize(&vec, &vec);
		// 大きめにとる
		localPos[nCnt] += (vec * 50.0f);

		D3DXVec3TransformCoord(&worldPos[nCnt], &localPos[nCnt], &mtxWorld);

	}

	D3DXVECTOR3 vecPlayer[4];

	// 頂点座標の取得
	vecPlayer[0] = *pPos - worldPos[0];
	vecPlayer[1] = *pPos - worldPos[1];
	vecPlayer[2] = *pPos - worldPos[2];
	vecPlayer[3] = *pPos - worldPos[3];

	D3DXVECTOR3 vecLine[4];

	// 四辺の取得 (v2)
	vecLine[0] = worldPos[1] - worldPos[0];
	vecLine[1] = worldPos[2] - worldPos[1];
	vecLine[2] = worldPos[3] - worldPos[2];
	vecLine[3] = worldPos[0] - worldPos[3];

	float InOut[4];

	InOut[0] = Vec2Cross(&vecLine[0], &vecPlayer[0]);
	InOut[1] = Vec2Cross(&vecLine[1], &vecPlayer[1]);
	InOut[2] = Vec2Cross(&vecLine[2], &vecPlayer[2]);
	InOut[3] = Vec2Cross(&vecLine[3], &vecPlayer[3]);

	D3DXVECTOR3 pos = GetPos();

	if (InOut[0] < 0.0f && InOut[1] < 0.0f && InOut[2] < 0.0f && InOut[3] < 0.0f)
	{
		if (pPos->y < pos.y + max.y && pPos->y + pSize->y > pos.y)
		{
			Hit();
			bIsLanding = true;
		}
	}

	return bIsLanding;
}

//------------------------------------
// 動き系統
//------------------------------------
void CBuilding::move()
{

	D3DXVECTOR3 Move = GetMove();
	MovePos(Move);
}

//------------------------------------
// 動き系統
//------------------------------------
void CBuilding::Hit()
{
	if (m_Gold)
	{//Goal設定
		CGameEnd::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH / 2, CManager::SCREEN_HEIGHT / 2, 0.0f),
			D3DXVECTOR3(600.0f, 400.0f, 0.0f),
			120,
			0.05f,
			CTexture::TEXTURE_GOAL);

		CPlayfab::SetScore(CNameSet::GetName(), CGame::GetTimer()->GetTime());
	}
	if (m_ChangePoptime)
	{//中間設定
		m_nCheckCounter++;			//確認変数を加算
		if (m_nCheckCounter == 1)
		{//確認変数がちょうど１だったら
			CManager::GetSound()->Play(CSound::SOUND_SE_CHECK_POINT);		//SEを流す
			m_pObj2D = CObject2D::Create(D3DXVECTOR3(1170.0f, 680.0f, 0.0f), D3DXVECTOR3(200.0f, 50.0f, 0.0f), PRIORITY_UI);
			m_pObj2D->SetTexture(CTexture::TEXTURE::TEXTURE_CHECKPOINT);
		}

		if (m_nCheckCounter >= 60)
		{
			if (m_pObj2D != nullptr)
			{
				m_pObj2D->Uninit();
				m_pObj2D = nullptr;
			}
		}
			
		CGame::GetStage()->GetPlayer()->SetChangePoptime(GetPos());
	}
}



